#!/usr/bin/env bash
# install for development environment
# ejb 19/9/17; update 9/10/19 - moved out of ./installDevelopment due to possible bad paths
#
# Set up all git-hook symbolic links
# to process headers during pre-/post-commits
#
#    Usage: ./makeGitHooks.sh [-d] [-h]
#
#        -d     Dry-run.
#        -h     Show the help message.
#
#    Use lowercase for yes/true, uppercase for no/false (if applicable).
#
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# import settings
source ./common.sh
source ./projectSettings.sh

# links for the commit scripts, and scripts to process file headers
hooksPath=".git/hooks"
links=(pre-commit
    post-commit
    CheckHeader.py
    Version.py
)
paths=("internal/src/python/git_hooks"
    "internal/src/python/git_hooks"
    "internal/src/python"
    "src/python/ccpn/framework"
)

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function make_git_hooks_func() {
    local thisRep thisFile
    local relativePath repNum
    local sourceFile

    while getopts ":dh" OPT; do
        case ${OPT} in
            d)
                [[ ${DRYRUN} ]] && die_getopts "-$(lower "${OPT}")|$(upper "${OPT}") already specified"
                DRYRUN="dryrun"
                ;;
            h)
                echo "Usage: $0 [-d] [-h]"
                echo "    -d              Dry-run."
                echo "    -h              Show the help message."
                echo "Use lowercase for yes/true, uppercase for no/false (if applicable)."
                exit
                ;;
            *)
                echo "Usage: $0 [-d] [-h]"
                echo "    -h    Show the help message."
                exit
                ;;
        esac
    done
    shift $((OPTIND - 1))

    # check whether using a Mac
    export_osx_dyld_path

    # remove any existing symbolic links and add new
    if [[ -d "${PROJECT_DEFAULT}" ]]; then
        # cleaning existing links
        echo "cleaning existing symbolic-links"
        cd "${PROJECT_DEFAULT}" || exit
        for thisFile in ${links[*]}; do
            ${DRYRUN} find . -name "${thisFile}" -type l -delete
        done

        # add new symbolic links
        # link is from ./internal/src/python/git_hooks/ to .git/hooks each of the repositories
        echo "creating symbolic-links"
        for ((repNum = 0; repNum < ${#REPOSITORY_NAMES[@]}; repNum++)); do
            thisRep=${REPOSITORY_RELATIVE_PATHS[${repNum}]}

            if [[ -d "${PROJECT_DEFAULT}/${thisRep}/${hooksPath}" ]]; then
                cd "${PROJECT_DEFAULT}/${thisRep}/${hooksPath}" || exit
                echo "Repository: ${PWD}"

                for ((ss = 0; ss < ${#links[@]}; ss++)); do
                    sLink=${links[${ss}]}
                    sPath=${paths[${ss}]}
                    sourceFile="${PROJECT_DEFAULT}/${sPath}/${sLink}"
                    relativePath=$(relative_path "${PROJECT_DEFAULT}/${sPath}" ".")
                    if [[ -f "${sourceFile}" ]]; then
                        echo " linking ${sLink}: ${relativePath} -> ${hooksPath}"
                        ${DRYRUN} make_link "${relativePath}/${sLink}" "${sLink}"
                    fi
                done
            else
                echo "${PROJECT_DEFAULT}/${thisRep}/${hooksPath} - not found"
            fi
        done
    fi
}

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

print_header "${PROJECT_TITLE} - Create symbolic-links for git-hooks"
make_git_hooks_func "$@"

echo "done"
