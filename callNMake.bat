rem Batch file to compile all the c code and copy to the relevant folders
rem e.g.
rem     nmake              to compile
rem     nmake realclean    to clean all files
rem     nmake cleanpyd     to clean .pyd files
@echo off
setlocal

set HERE=%~dp0
set WIN_CMD=C:\Program Files\Microsoft Visual Studio\2022\Community\VC\Auxiliary\Build\vcvars64.bat
rem Call the batch to set up the x64 native vs2019 environment
call "%WIN_CMD%"
call "%HERE%\paths.bat"

rem Call NMake to compile c code
:ARGS
nmake %1

shift
rem Loop through all parameters
if not "%1" == "" goto ARGS

endlocal
