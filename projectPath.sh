#!/usr/bin/env bash
#
# To change the project location change the settings in here
#   - it is sourced by ./projectSettings and from there to the other scripts
#
# ejb
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

export PROJECT_NAME=AnalysisV3
export PROJECT_TITLE="CcpNmr ${PROJECT_NAME}"

# project installation path
export PROJECT_DEFAULT=${HOME}/Projects/${PROJECT_NAME}

# *** remember to set the matching value in ./projectPath.bat if using Windows