#!/usr/bin/env bash
# Display the machine information
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# add a trap to exit script on error 99
trap "exit 99" ERR

# import settings
source ./common.sh

echo "OS=$(get_machine_type)"
echo "OS_TYPE=$(detect_os)"
echo "CPU_TYPE=$(detect_arch)"
echo "NAME=$(os_name)"
echo "VERSION=$(os_version)"
