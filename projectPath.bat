@echo off

set PROJECT_NAME=AnalysisV3
set PROJECT_TITLE="CcpNmr %PROJECT_NAME%"

rem this MUST match PROJECT_DEFAULT in ./projectPaths.sh if using Windows
set PROJECT_DEFAULT=%HOME%\Projects\%PROJECT_NAME%
