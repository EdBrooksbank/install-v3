#!/usr/bin/env bash

# installation specific information
#
# To change the project location edit the settings in ./projectPath.sh
#
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# import project path
source ./common.sh
source ./version.sh
source ./projectPath.sh

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# build paths
export CONDA_SCRIPT_PATH=miniconda_scripts
export COMPILE_SCRIPTS=compile_scripts
export VERSION_PATH=src
export C_PATH=c

export SKIP_SCRIPTS_LIST=(assign screen structure metabolomics)
if is_windows; then
    # do the os-specific checks here
    export INCLUDE_DIRS="bat config doc resources ${VERSION_PATH} tutorials .updates"
else
    export INCLUDE_DIRS="bin config doc resources ${VERSION_PATH} tutorials .updates"
fi
export DATA_DIR="data"
export INCLUDE_FILES="DISCLAIMER.txt LGPL.license LICENSE.txt README.txt"
export SKIP_REPOSITORIES=(NEF)
INSTALLSCRIPT_PATH="$(script_path)"
export INSTALLSCRIPT_PATH
export CONDA_YML_PATH="${INSTALLSCRIPT_PATH}/${CONDA_SCRIPT_PATH}"

export PYQT="PyQt5"
export MAC_PYTHON_VERSION="3.10"
if is_windows; then
    # when first implemented windows was different - will check again
    export DEFAULT_CONDA=miniconda3
else
    export DEFAULT_CONDA=miniconda3
fi
export ENV_PATH=miniconda
export CONDA_EXE=conda
export CONDA=${PROJECT_DEFAULT}/${ENV_PATH}
# not actually using now as keep getting weird errors
alias condaAlias="conda"

# Makefiles that need copying to the correct paths
export MAKEFILE_NAMES=(Makefile_c
    Makefile_c_ccpnc
    Makefile_c_ccpnc_clibrary
    Makefile_c_ccpnc_contour
    Makefile_c_ccpnc_peak)

export MAKEFILE_RELATIVE_PATHS=("${C_PATH}"
    "${C_PATH}/ccpnc"
    "${C_PATH}/ccpnc/clibrary"
    "${C_PATH}/ccpnc/contour"
    "${C_PATH}/ccpnc/peak")

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# global variables for external packages
export FONTCONFIG_FILE="${CONDA}"/etc/fonts/fonts.conf
export FONTCONFIG_PATH="${CONDA}"/etc/fonts
export QT_PLUGIN_PATH=${QT_PLUGIN_PATH}:"${CONDA}"/plugins
export BITBUCKET_WEBSITE=bitbucket.org

# leave PYTHONPATH without underscore
export PYTHONPATH=${PROJECT_DEFAULT}/src/python:${PROJECT_DEFAULT}/src/c

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# repositories contained in the project
export REPOSITORY_NAMES=(analysis
    data
    internal
    ccpnmodel
    analysisassign
    analysisstructure
    analysisscreen
    analysismetabolomics
    nefio
    NEF)

export REPOSITORY_RELATIVE_PATHS=(""
    /data
    /internal
    "/${VERSION_PATH}/python/ccpnmodel"
    "/${VERSION_PATH}/python/ccpn/AnalysisAssign"
    "/${VERSION_PATH}/python/ccpn/AnalysisStructure"
    "/${VERSION_PATH}/python/ccpn/AnalysisScreen"
    "/${VERSION_PATH}/python/ccpn/AnalysisMetabolomics"
    "/${VERSION_PATH}/python/ccpn/util/nef"
    "/${VERSION_PATH}/python/ccpn/util/nef/NEF")

export REPOSITORY_SOURCE=(git@bitbucket.org:ccpnmr
    git@bitbucket.org:ccpnmr
    git@bitbucket.org:ccpnmr
    git@bitbucket.org:ccpnmr
    git@bitbucket.org:ccpnmr
    git@bitbucket.org:ccpnmr
    git@bitbucket.org:ccpnmr
    git@bitbucket.org:ccpnmr
    git@bitbucket.org:ccpnmr
    https://github.com/NMRExchangeFormat)

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# not in common as requires the above definitions
function export_osx_dyld_path() {
    # check if using a Mac
    if [[ "$(uname -s)" == 'Darwin*' ]]; then
        export DYLD_FALLBACK_LIBRARY_PATH=/System/Library/Frameworks/ApplicationServices.framework/Versions/A/Frameworks/ImageIO.framework/Versions/A/Resources:
        export DYLD_FALLBACK_LIBRARY_PATH=${DYLD_FALLBACK_LIBRARY_PATH}${CONDA}/lib:
        export DYLD_FALLBACK_LIBRARY_PATH=${DYLD_FALLBACK_LIBRARY_PATH}${CONDA}/lib/python${MAC_PYTHON_VERSION}/site-packages/${PYQT}:
        export DYLD_FALLBACK_LIBRARY_PATH=${DYLD_FALLBACK_LIBRARY_PATH}${HOME}/lib:/usr/local/lib:/usr/lib
    fi
}
