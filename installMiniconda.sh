#!/usr/bin/env bash
# Install miniconda and apply patches
# ejb 19/9/17
#
# Remember to check out the required release in Pycharm, or manually with git in each repository.
#
# download and install the miniconda package and create the required environment for project
#
# to make a patch
# reinstall pyqtgraph first: conda install -f pyqtgraph
# copy the required file to a new file, make alterations
# generate difference
#   diff -u <newfile> <oldfile>
# automatic culling of whitespace must be disabled in editor (Atom specifically) if viewing these files
# copy/paste into patch file below
#
#    Usage: ./installMiniconda.sh [-a|A] [-d] [-e|E] [-f] [-h] [-i|I] [-l|L] [-m|M] [-p <path>] [-P] [-r|R] [u|U] [-y]
#
#        -a|A        Apply patches (yes/no).
#        -d          Dry-run.
#        -e|E        Install new conda environment (yes/no).
#        -f          Force overwrite of existing miniconda installation.
#        -h          Show the help message.
#        -i|I        Install miniconda package (yes/no).
#        -l|L        Create symbolic link in development folder (yes/no).
#        -m|M        Download the latest miniconda (yes/no).
#        -p <path>   Specify conda path.
#        -P          Use default conda path [${condaDefault}].
#        -r|R        Reset the base conda environment (yes/no).
#        -u|U        Update the conda environment (yes/no).
#        -y          Answer 'yes' to prompts that are not explicitly set.
#
#    Use lowercase for yes/true, uppercase for no/false (if applicable).
#    Arguments not set will request input unless -y specified.
#
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# add a trap to exit script on error 99
trap "exit 99" ERR
# needed for the alias in projectSettings.sh to work correctly
shopt -s expand_aliases

# import settings
source ./common.sh
source ./projectSettings.sh

if is_windows; then
    # Windows needs to .exe extension, Mac/Linux use .sh
    extension=".exe"
else
    extension=".sh"
fi

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# make a heading
print_header "${PROJECT_TITLE} - Install conda/pip environment"
echo "installing from ${INSTALLSCRIPT_PATH}"

machine=$(get_machine)
repoLabel=$(get_machine_type)
repoLabelPostfix=$(get_machine_append)
bitCount=$(get_bit_count)
echo "current machine: ${machine}, ${bitCount}"
echo "OS=$(get_machine_type)"
echo "OS_TYPE=$(detect_os)"
echo "CPU_TYPE=$(detect_arch)"
echo "NAME=$(os_name)"
echo "VERSION=$(os_version)"

# settings for the latest version of conda
condaWebsite="http://repo.continuum.io"
condaVersion="Miniconda3-latest-${repoLabel}${repoLabelPostfix}-${bitCount}"
condaDefault="${HOME}/${DEFAULT_CONDA}"
condaPackage="${DEFAULT_CONDA}"

# process arguments
while getopts ":aAbBdeEfhiIlLmMp:PrRuUy" OPT; do
    case ${OPT} in
        a)
            [[ ${applyPatchesArg} ]] && die_getopts "-$(lower "${OPT}")|$(upper "${OPT}") already specified"
            applyPatchesArg="yes"
            ;;
        A)
            [[ ${applyPatchesArg} ]] && die_getopts "-$(lower "${OPT}")|$(upper "${OPT}") already specified"
            applyPatchesArg="no"
            ;;
        d)
            [[ ${DRYRUN} ]] && die_getopts "-$(lower "${OPT}")|$(upper "${OPT}") already specified"
            DRYRUN="dryrun"
            ;;
        e)
            [[ ${newEnvArg} ]] && die_getopts "-$(lower "${OPT}")|$(upper "${OPT}") already specified"
            newEnvArg="yes"
            ;;
        E)
            [[ ${newEnvArg} ]] && die_getopts "-$(lower "${OPT}")|$(upper "${OPT}") already specified"
            newEnvArg="no"
            ;;
        f)
            [[ ${forceArg} ]] && die_getopts "-${OPT} already specified"
            forceArg="yes"
            ;;
        h)
            echo "Usage: $0 [-a|A] [-d] [-e|E] [-f] [-h] [-i|I] [-l|L] [-m|M] [-p <path>] [-P] [-r|R] [u|U] [-y]"
            echo "    -a|A        Apply patches (yes/no)."
            echo "    -d          Dry-run."
            echo "    -e|E        Install new conda environment (yes/no)."
            echo "    -f          Force overwrite of existing miniconda installation."
            echo "    -h          Show the help message."
            echo "    -i|I        Install miniconda package (yes/no)."
            echo "    -l|L        Create symbolic-link in development folder (yes/no)."
            echo "    -m|M        Download the latest miniconda (yes/no)."
            echo "    -p <path>   Specify conda path."
            echo "    -P          Use default conda path [${condaDefault}]."
            echo "    -r|R        Reset the base conda environment (yes/no)."
            echo "    -u|U        Update the conda environment (yes/no)."
            echo "    -y          Answer 'yes' to prompts that are not explicitly set."
            echo "Use lowercase for yes/true, uppercase for no/false (if applicable)."
            echo "Arguments not set will request input unless -y specified."
            exit
            ;;
        i)
            [[ ${installArg} ]] && die_getopts "-$(lower "${OPT}")|$(upper "${OPT}") already specified"
            installArg="yes"
            ;;
        I)
            [[ ${installArg} ]] && die_getopts "-$(lower "${OPT}")|$(upper "${OPT}") already specified"
            installArg="no"
            ;;
        l)
            [[ ${minicondaLinkArg} ]] && die_getopts "-$(lower "${OPT}")|$(upper "${OPT}") already specified"
            minicondaLinkArg="yes"
            ;;
        L)
            [[ ${minicondaLinkArg} ]] && die_getopts "-$(lower "${OPT}")|$(upper "${OPT}") already specified"
            minicondaLinkArg="no"
            ;;
        m)
            [[ ${downloadArg} ]] && die_getopts "-$(lower "${OPT}")|$(upper "${OPT}") already specified"
            downloadArg="yes"
            ;;
        M)
            [[ ${downloadArg} ]] && die_getopts "-$(lower "${OPT}")|$(upper "${OPT}") already specified"
            downloadArg="no"
            ;;
        p)
            [[ ${condaPathArg} ]] && die_getopts "-$(lower "${OPT}")|$(upper "${OPT}") already specified"
            condaPathArg=${OPTARG}
            minicondaLinkSwitches="${minicondaLinkSwitches} -${OPT} ${OPTARG}"
            ;;
        P)
            [[ ${condaPathArg} ]] && die_getopts "-$(lower "${OPT}")|$(upper "${OPT}") already specified"
            condaPathArg=${condaDefault}
            minicondaLinkSwitches="${minicondaLinkSwitches} -${OPT}"
            ;;
        r)
            [[ ${resetArg} ]] && die_getopts "-$(lower "${OPT}")|$(upper "${OPT}") already specified"
            resetArg="yes"
            ;;
        R)
            [[ ${resetArg} ]] && die_getopts "-$(lower "${OPT}")|$(upper "${OPT}") already specified"
            resetArg="no"
            ;;
        u)
            [[ ${updateArg} ]] && die_getopts "-$(lower "${OPT}")|$(upper "${OPT}") already specified"
            updateArg="yes"
            ;;
        U)
            [[ ${updateArg} ]] && die_getopts "-$(lower "${OPT}")|$(upper "${OPT}") already specified"
            updateArg="no"
            ;;
        y)
            [[ ${yesArg} ]] && die_getopts "-${OPT} already specified"
            yesArg="yes"
            ;;
        *)
            echo "Usage: $0 [-a|A] [-d] [-e|E] [-f] [-h] [-i|I] [-l|L] [-m|M] [-p <path>] [-P] [-r|R] [u|U] [-y]"
            echo "    -h          Show the help message."
            exit
            ;;
    esac
done
shift $((OPTIND - 1))

#if is_windows; then
#    echo "Please install Miniconda manually - miniconda causes many problems for this installer :)"
#    echo "and select 'n' to installing a new version of miniconda"
#    if [[ ! ${yesArg} ]]; then
#        continue_prompt "If you have already installed Miniconda, would you like to continue?"
#    fi
#fi

condaPath=${condaPathArg:-$(read_default "Please enter ${condaPackage} installation path [${condaDefault}]:" "${condaDefault}")}
condaPath=${condaPath:-$condaDefault}
condaEnvs="${condaPath}/envs"
condaEnvPath="${condaPath}/envs/${CONDA_SOURCE}"
condaFile="${condaVersion}${extension}"
#condaFile="install.sh"  # for micromamba

# ask to download the latest conda
ans=${downloadArg:-$yesArg}
ans=${ans:-$(yesno_prompt "Do you want to download the latest ${condaPackage}?")}

if [[ ${ans} == "yes" ]]; then
    # download a new version of conda
    echo "checking website for file ${condaFile}"
    if ! curl --output /dev/null --silent --head --fail --connect-timeout 3 "${condaWebsite}"; then
        echo "miniconda download page is not responding, please try later"
        exit
    elif [[ ! ${yesArg} ]]; then
        continue_prompt "URL active, continue with download?"
    fi

    echo "downloading ${condaFile}"
    cd "${CONDA_YML_PATH}" || exit
    if is_windows; then
        # sometimes causes problems with Anaconda install
        ${DRYRUN} /usr/bin/rm -f "${condaFile}" || exit
    else
        ${DRYRUN} rm -f "${condaFile}" || exit
    fi

    # download the file
    if command_exists wget; then
        ${DRYRUN} wget -c --no-check-certificate "${condaWebsite}/miniconda/${condaFile}"
    elif command_exists curl; then
        ${DRYRUN} curl -O -L "${condaWebsite}/miniconda/${condaFile}"
    else
        echo "Error: neither wget nor curl exist, please install one and run again."
        exit
    fi

    if [[ ! -f "${condaFile}" && ! ${DRYRUN} ]]; then
        echo "Error - not downloaded"
        echo "if you have the file ${condaFile}, copy it to this directory and try again"
        exit
    fi
    # change permissions to executable
    ${DRYRUN} chmod +x "${condaFile}"
fi

# ask to install miniconda, from either the latest download, or already in the folder
ans=${installArg:-$yesArg}
ans=${ans:-$(yesno_prompt "Do you want to install miniconda?")}

if [[ ${ans} == "yes" ]]; then
    # preparing miniconda directory
    # miniconda install will exit if path exists
    # this assumes that you have already installed in the default location
    # any other directory will need to be manually deleted
    if [[ ! ${forceArg} ]]; then
        if [[ -d "${condaPath}" ]]; then
            cd "${HOME}" || exit
            if [[ ! ${yesArg} ]]; then
                continue_prompt "${condaPath} already exists, do you want to continue (will rename)?"
            fi

            dateTime=$(date '+%d-%m-%Y_%H:%M:%S')
            echo "renaming miniconda path to ${condaPath}_${dateTime}"
            ${DRYRUN} mv -f "${condaPath}" "${condaPath}_${dateTime}" || exit
        fi

        # installing miniconda
        echo "installing miniconda"
        cd "${CONDA_YML_PATH}" || exit
        if is_windows; then
            ${DRYRUN} cmd <<< "start /wait "" .\\${condaFile} /InstallationType=JustMe /AddToPath=1 /S /D=\"${condaPath}\"" > /dev/null
        else
            # silent install - Windows needs different settings
            ${DRYRUN} ./"${condaFile}" -b -p "${condaPath}"
        fi
    else

        # installing miniconda
        #   - force installation to existing path
        echo "force-installing miniconda"
        cd "${CONDA_YML_PATH}" || exit
        if is_windows; then
            ${DRYRUN} cmd <<< "start /wait "" .\\${condaFile} /InstallationType=JustMe /AddToPath=1 /S /D=\"${condaPath}\"" > /dev/null
        else
            # silent install - Windows needs different settings
            ${DRYRUN} ./"${condaFile}" -b -f -p "${condaPath}"
        fi
    fi
fi
if ! hash conda >/dev/null 2>&1; then
    # still hard-coded to conda here
    if is_windows; then
        echo "${condaPath}"
        "${condaPath}"/condabin/conda.bat init bash
    else
        # insert the paths into the bash profile
        "${condaPath}"/bin/conda init bash
    fi
fi
if command_exists conda; then
    echo "alias found - ${condaPath} initialised"
fi
# re-execute that shell script to make sure the paths are activated
if [[ -f "${HOME}/.bash_profile" ]]; then
    echo "executing ~/.bash_profile"
    source "${HOME}/.bash_profile" || exit
fi

# reset base conda environment to latest
if [[ ${yesArg} ]]; then
    yesFlagUpdate="-y"
fi
resetAns=${resetArg:-$yesArg}
resetAns=${resetAns:-$(yesno_prompt "Do you want to reset ${condaPackage}?")}
updateAns=${updateArg:-$yesArg}
updateAns=${updateAns:-$(yesno_prompt "Do you want to update ${condaPackage}?")}

if [[ ${resetAns} == "yes" ]]; then
    ${DRYRUN} conda install --revision 1 --name base ${yesFlagUpdate}
fi

# install the libmamba quick solver - I think is now in new miniconda builds
conda config --prepend channels conda-forge 2> /dev/null
if ! conda list -n base conda-libmamba-solver | grep conda-libmamba-solver; then
    ${DRYRUN} conda install -n base ${yesFlagUpdate} conda-libmamba-solver
elif [[ ${updateAns} == "yes" ]]; then
    ${DRYRUN} conda update -n base ${yesFlagUpdate} conda-libmamba-solver
fi
if ! conda config --show | grep libmamba; then
    ${DRYRUN} conda config --set solver libmamba
fi

# install a new environment
ans=${newEnvArg:-$yesArg}
ans=${ans:-$(yesno_prompt "Do you want to install a new environment?")}

if [[ ${ans} == "yes" ]]; then
    # create CcpNmr environment
    ${DRYRUN} source activate base
    cd "${CONDA_YML_PATH}" || exit

    if is_windows || is_linux; then
        envYml="environment_$(lower "$(os_name)")$(os_version).yml"
    elif is_macos; then
        if [[ $(detect_arch) == "arm64" ]]; then
            envYml="environment_$(lower "$(os_name)")m.yml"
        else
            envYml="environment_$(lower "$(os_name)").yml"
        fi
    else
        echo "Error: machine not found"
        exit 99
    fi
    if [[ ! -f ${envYml} ]]; then
        echo "cannot find ${envYml} - using os-default"
        # fall-back to no version
        envYml="environment_$(detect_os).yml"
    fi
    if [[ ! -f ${envYml} ]]; then
        echo "Error: cannot find os-default ${envYml}"
        exit 99
    fi
    echo "creating environment from ${envYml}"

    # copy required environment and insert correct source into 4th line to keep comments
    condaHeader="name: ${CONDA_SOURCE}"
    if [[ ! ${DRYRUN} ]]; then
        (
            head -n 3 "${envYml}"
            echo "${condaHeader}"
            tail -n +5 "${envYml}"
        ) > environment.yml
    fi

    if [[ -d "${condaEnvPath}" ]]; then
        ${DRYRUN} conda env remove ${yesFlagUpdate} -n "${CONDA_SOURCE}"

        if is_windows; then
            # sometimes causes problems with Anaconda install - may need to delete the folder
            cd "${condaEnvs}" || exit
            echo "env - ${condaEnvs} -> ${CONDA_SOURCE}"
            ${DRYRUN} /usr/bin/rm -rf "${CONDA_SOURCE}" || exit
        fi
    fi
    cd "${CONDA_YML_PATH}" || exit
    ${DRYRUN} conda config --set ssl_verify true || exit

    echo "creating env"
    if [[ ${yesArg} ]]; then
        yesFlagCreate="-y"
    fi
    ${DRYRUN} conda env create ${yesFlagCreate} -f "environment.yml" || exit
    if [[ ! -d "${condaEnvPath}" ]]; then
        echo "Error: missing path installing ${CONDA_SOURCE} from ${envYml}"
        exit
    fi
#    conda config --set ssl_verify true

    cd "${CONDA_YML_PATH}" || exit
    # activate env which sets path to <condainstallpath/envs/current environment> then strip off 'python'
    ${DRYRUN} source activate "${CONDA_SOURCE}"
fi

# create symbolic link as required
ans=${minicondaLinkArg:-$yesArg}
ans=${ans:-$(yesno_prompt "Do you want to create symbolic link?")}

if [[ ${ans} == "yes" ]]; then
    # remove old link and make new link
    cd "${INSTALLSCRIPT_PATH}" || exit
    ${DRYRUN} ./makeMinicondaLink.sh "${minicondaLinkSwitches}" || exit
fi

# apply patches as required
ans=${applyPatchesArg:-$yesArg}
ans=${ans:-$(yesno_prompt "Do you want to apply patches?")}

if [[ ${ans} == "yes" ]]; then
    # apply the site-packages patches
    cd "${INSTALLSCRIPT_PATH}" || exit
    ${DRYRUN} source ./patchSitePackages.sh
fi

# notify to compile C Code
echo "done - please run ./compileCCode.sh to finish"
