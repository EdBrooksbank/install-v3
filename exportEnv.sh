# shellcheck source=~
# Output the current micromamba env to a yaml file.
#   required as the standard env export does not include the pip packages
#
#    Usage: ./exportEnv.sh [-d|D] [-h] [-p|P]
#
#        -d|D   include/exclude packages with dependencies
#        -p|P   include/exclude pip-packages.
#        -h     Show the help message.
#
#    Use lowercase for yes/true, uppercase for no/false (if applicable).
#    Arguments not set will request input.
#
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# add a trap to exit script on error 99
trap "exit 99" ERR
# needed for the alias in projectSettings.sh to work correctly
shopt -s expand_aliases

# import settings
source ./common.sh
source ./projectSettings.sh

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# make a heading
print_header "Export ${} Env"

# initialise settings
source ~/.bash_profile
micromamba activate "${CONDA_SOURCE}"

# process arguments
while getopts ":dDpPh" OPT; do
    case ${OPT} in
        d)
            [[ ${dependenciesArg} ]] && die_getopts "-$(lower "${OPT}")|$(upper "${OPT}") already specified"
            dependenciesArg=
            ;;
        D)
            [[ ${dependenciesArg} ]] && die_getopts "-$(lower "${OPT}")|$(upper "${OPT}") already specified"
            dependenciesArg="--not-required"
            ;;
        h)
            echo "Usage: $0 [-d|D] [-h] [-p|P]"
            echo "    -d|D   include/exclude packages with dependencies"
            echo "    -p|P   include/exclude pip-packages."
            echo "    -h     Show the help message."
            echo "Use lowercase for yes/true, uppercase for no/false (if applicable)."
            echo "Arguments not set will request input."
            exit
            ;;
        p)
            [[ ${pipArg} ]] && die_getopts "-$(lower "${OPT}")|$(upper "${OPT}") already specified"
            pipArg="yes"
            ;;
        P)
            [[ ${pipArg} ]] && die_getopts "-$(lower "${OPT}")|$(upper "${OPT}") already specified"
            pipArg="no"
            ;;
        *)
            echo "Usage: $0 [-d|D] [-h] [-p|P]"
            echo "    -h    Show the help message."
            exit
            ;;
    esac
done
shift $((OPTIND - 1))
pipArg=${pipArg:-yes}
conda=$(micromamba list | tail -n +5 | grep .)
pip list "${dependenciesArg}" | tail -n +3 | merge_conda_pip "${conda}"
exit

(
    micromamba env export --no-build | grep .
    if [[ ${pipArg} == "yes" ]]; then
        echo "- pip:"
#        pip freeze | grep "==" | sed 's/^/  - /'
        pip list "${dependenciesArg}" | tail -n +3 | sed 's/^/- /'
    fi
) > "./miniconda_scripts/environment_$(get_build).yml"
