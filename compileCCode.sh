#!/usr/bin/env bash
# Compile C Code for the current environment, should be executed from the end of installMiniconda
# ejb 19/9/17
#
# Remember to check out the required release in Pycharm, or manually with git in each repository.
#
# recompile the c code.
#
#    Usage: ./compileCCode.sh [-b|B] [-c|C] [-d] [-h]
#
#        -b|B              Backup the Makefiles (yes/no).
#        -c <conda-path>   Specify the full-path to the conda environment.
#        -C                Use default conda path [${condaDefault}].
#        -d                Dry-run.
#        -h                Show the help message.
#
#    Use lowercase for yes/true, uppercase for no/false (if applicable).
#    Arguments not set will request input.
#
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# add a trap to exit script on error 99
trap "exit 99" ERR

# import settings
source ./common.sh
source ./projectSettings.sh

# initialise settings
windowsCompile="${INSTALLSCRIPT_PATH}/callNMake.bat"
condaDefault="${HOME}/${DEFAULT_CONDA}"
backupMakefilesDefault="yes"

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# make a heading
print_header "${PROJECT_TITLE} - Compile C-code"

# process arguments
while getopts ":bBc:Cdh" OPT; do
    case ${OPT} in
        b)
            [[ ${backupMakefilesArg} ]] && die_getopts "-$(lower "${OPT}")|$(upper "${OPT}") already specified"
            backupMakefilesArg="yes"
            ;;
        B)
            [[ ${backupMakefilesArg} ]] && die_getopts "-$(lower "${OPT}")|$(upper "${OPT}") already specified"
            backupMakefilesArg="no"
            ;;
        c)
            [[ ${condaPathArg} ]] && die_getopts "-$(lower "${OPT}")|$(upper "${OPT}") already specified"
            condaPathArg=${OPTARG}
            ;;
        C)
            [[ ${condaPathArg} ]] && die_getopts "-$(lower "${OPT}")|$(upper "${OPT}") already specified"
            condaPathArg=${condaDefault}
            ;;
        d)
            [[ ${DRYRUN} ]] && die_getopts "-$(lower "${OPT}")|$(upper "${OPT}") already specified"
            DRYRUN="dryrun"
            ;;
        h)
            echo "Usage: $0 [-b|B] [-c|C] [-d] [-h]"
            echo "    -b|B              Backup the Makefiles (yes/no)."
            echo "    -c <conda-path>   Specify the full-path to the conda environment."
            echo "    -C                Use default conda path [${condaDefault}]."
            echo "    -d                Dry-run."
            echo "    -h                Show the help message."
            echo "Use lowercase for yes/true, uppercase for no/false (if applicable)."
            echo "Arguments not set will request input."
            exit
            ;;
        *)
            echo "Usage: $0 [-b|B] [-c|C] [-d] [-h]"
            echo "    -h    Show the help message."
            exit
            ;;
    esac
done
shift $((OPTIND - 1))

# Get the local machine type
machine=$(get_machine)
echo "current machine: ${machine}"

# check whether using a Mac
export_osx_dyld_path

# re-execute that shell script to make sure the paths are activated
if [[ -f "${HOME}/.bash_profile" ]]; then
    echo "executing ~/.bash_profile"
    source "${HOME}/.bash_profile" || exit
fi

# read the miniconda path option
condaPath=${condaPathArg:-$(read_valid_path "Please enter miniconda installation path [${condaDefault}]:" "${condaDefault}")}
if [[ ! -d "${condaPath}" ]]; then
    echo "Error - miniconda path not found or not a directory"
    exit
fi
# read the backup-makefiles option
backupMakefiles="${backupMakefilesArg:-$(yesno_prompt "Do you want to backup Makefiles?")}"
backupMakefiles="${backupMakefiles:-$backupMakefilesDefault}"

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
echo "using ${condaPath} ${backupMakefiles}"

condaEnvPath="${condaPath}/envs/${CONDA_SOURCE}"
cd "${PROJECT_DEFAULT}" || exit
if [[ ! -d "${condaEnvPath}" ]]; then
    echo "Error compiling - conda environment ${CONDA_SOURCE} does not exist"
    exit
fi

# make a symbolic link for the miniconda path (if it does not exists)
if [[ ! -d miniconda ]]; then
    echo "Creating miniconda symbolic link"
    ${DRYRUN} make_link "${condaEnvPath}" miniconda || exit
fi

# copy the Makefiles to the correct place
cd "${INSTALLSCRIPT_PATH}/${COMPILE_SCRIPTS}" || exit

echo "copying Makefiles"
for ((makeNum = 0; makeNum < ${#MAKEFILE_NAMES[@]}; makeNum++)); do

    # concatenate paths to give the correct Makefile path
    thisMake=${MAKEFILE_NAMES[${makeNum}]}
    relativePath=${MAKEFILE_RELATIVE_PATHS[${makeNum}]}
    makePath=${PROJECT_DEFAULT}/${VERSION_PATH}/${relativePath}/Makefile

    dateTime=$(date '+%d-%m-%Y_%H:%M:%S')
    if [[ -f "${makePath}" && ${backupMakefiles} == "yes" ]]; then
        echo "backup ${makePath} -> ${makePath}_${dateTime}"
        ${DRYRUN} mv "${makePath}" "${makePath}_${dateTime}" || exit
    fi
    ${DRYRUN} cp "${thisMake}" "${makePath}" || exit
done

# copy the correct environment file
cd "${PROJECT_DEFAULT}/${VERSION_PATH}/${C_PATH}" || exit

if is_macos && [[ $(detect_arch) == "arm64" ]]; then
    envTxt="environment_${machine}m.txt"
else
    envTxt="environment_${machine}.txt"
fi
echo "using ${envTxt}"
if [[ ! -f "${INSTALLSCRIPT_PATH}/${COMPILE_SCRIPTS}/${envTxt}" ]]; then
    echo "environment doesn't exist with this name, please copy closest and re-run compileCCode"
    exit
fi

# run 'make'
echo "setting up environment file"

# copy the required environment for the makefile
if is_windows; then
    # change to Windows path
    condaHeader="PYTHON_DIR = $(windows_path "${CONDA}")"
else
    condaHeader="PYTHON_DIR = ${CONDA}"
fi
condaHeaderEnv="CONDA_ENV = ${CONDA_SOURCE}"

# insert the PYTHON_DIR and CONDA_ENV into the first line of the environment file (CONDA_ENV not strictly required)
if [[ ! ${DRYRUN} ]]; then
    (
        echo "${condaHeader}"
        echo "${condaHeaderEnv}"
        tail -n +3 "${INSTALLSCRIPT_PATH}/${COMPILE_SCRIPTS}/${envTxt}"
    ) > environment.txt || exit
fi

echo "making path ${PROJECT_DEFAULT}/${VERSION_PATH}/${C_PATH}"
if is_windows; then
    # call Windows nmake, opens a windows shell with the x64 native vs2019/22 community .bat file
    nmakePath=$(windows_path "${windowsCompile}")
    ${DRYRUN} "${nmakePath}" "$@"
else
    ${DRYRUN} make -B "$@"
fi

echo "done"

# testing code - keep for now
#    CONDA="$(echo "${CONDA}" | sed -e 's/^\///' -e 's/\//\\/g' -e 's/^./\0:/')"
#    cmd <<< "call \"${nmakePath}\""
#    windowsCmdPath=$(windows_path "${windowsCmd}")
#    windowsPath=$(windows_path "${PROJECT_DEFAULT}/${VERSION_PATH}/${C_PATH}")
#    # this works but has a nested windows x64 native shell
#    cmd <<< "call \"${windowsCmdPath}\" && nmake"
#    cmd <<< "call \"${windowsCmdPath}\" && cd \"${windowsPath}\" && cd .. && dir"
