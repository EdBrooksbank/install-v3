rem Batch file to deep clean all the Windows .pyd files
@echo off
setlocal

set HERE=%~dp0
set WIN_CMD=C:\Program Files\Microsoft Visual Studio\2022\Community\VC\Auxiliary\Build\vcvars64.bat
rem Call the batch to set up the x64 native vs2019 environment
call "%WIN_CMD%"
call "%HERE%\paths.bat"

rem Call NMake to deep clean all the c code
nmake realclean
rem nmake cleanpyd

endlocal
