#!/usr/bin/env bash
# Install miniconda and apply patches
# ejb 19/9/17
#
# Remember to check out the required release in Pycharm, or manually with git in each repository.
#
# download and install the miniconda package and create the required environment for AnalysisV3
#
# to make a patch
# reinstall pyqtgraph first: conda install -f pyqtgraph
# copy the required file to a new file, make alterations
# generate difference
#   diff -u <newfile> <oldfile>
# automatic culling of whitespace must be disabled in editor (Atom specifically) if viewing these files
# copy/paste into patch file below
#
# also applies patch to pyqode - bad icon_type: PATH
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# import settings
source ./common.sh
source ./projectSettings.sh

# apply the site-packages patches - pyqtGraph/pyqode
# keep for the minute - may require again if pyqtgraph/pyqode give more errors
echo "applying patches"
echo "Not required for this env - ${CONDA_SOURCE}"

#cd "${PROJECT_DEFAULT}/miniconda" || exit
#if is_windows; then
#    /usr/bin/patch -tp0 < "${PROJECT_DEFAULT}/${CONDA_PATCH_PATH}/v3_0.10.0.pyqtgraph.Dos.patch" || exit
#    /usr/bin/patch -tp0 < "${PROJECT_DEFAULT}/${CONDA_PATCH_PATH}/v3_2.12.1.pyqode.Dos.patch" || exit
#else
#    patch -tp0 < "${PROJECT_DEFAULT}/${CONDA_PATCH_PATH}/v3_0.10.0.pyqtgraph.patch" || exit
#    patch -tp0 < "${PROJECT_DEFAULT}/${CONDA_PATCH_PATH}/v3_2.12.1.pyqode.patch" || exit
#fi
