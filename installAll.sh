#!/usr/bin/env bash
# install for development environment
#
# Download all repositories for Project.
# Install miniconda
# Set symbolic links
# Compile C Code
#
#    Usage: $0 [-c|C] [-d] [-g|G] [-h] [-l|L] [-m|M] [-r|R] [-s|S]
#
#        -c|C    Compile C Code (yes/no).                    [ccodeDefault]
#        -d      Dry-run.
#        -g|G    Make git-hooks (yes/no).                    [githooksDefault]
#        -h      Show the help message.
#        -l|L    Make miniconda symbolic-link (yes/no).      [minicondaLinkDefault]
#        -m|M    Install miniconda (yes/no).                 [minicondaDefault]
#        -r|R    Install development repositories (yes/no).  [develDefault]
#        -s|S    Generate ssh-key (yes/no).                  [sshkeyDefault]
#
#    Use lowercase for yes/true, uppercase for no/false (if applicable).
#    Arguments not set will revert to default settings.
#
#    Packages are installed in the following order:
#
#        1  generateSshKey.sh
#        2  installMiniconda.sh
#        3  installDevelopment.sh
#        4  makeGitHooks.sh
#        5  makeMinicondaLink.sh
#        6  compileCCode.sh
#
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# add a trap to exit script on error 99
trap "exit 99" ERR

# import settings
source ./common.sh
source ./projectSettings.sh

# set defaults/constants
# assume that ./generateSshKey.sh has been called first
# add all other install are required
ccodeDefault="yes"
develDefault="yes"
githooksDefault="yes"
minicondaDefault="yes"
minicondaLinkDefault="yes"
sshkeyDefault="no"

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function install_all_func() {
    # process arguments
    local ccodeArg
    local DRYRUN dryrunSwitch
    local githooksArg
    local minicondaLinkArg minicondaArg
    local develArg
    local sshkeyArg
    local OPT OPTIND OPTARG

    while getopts ":cCdgGhlLmMrRsS" OPT; do
        case ${OPT} in
            c)
                [[ ${ccodeArg} ]] && die_getopts "-$(lower "${OPT}")|$(upper "${OPT}") already specified"
                ccodeArg="yes"
                ;;
            C)
                [[ ${ccodeArg} ]] && die_getopts "-$(lower "${OPT}")|$(upper "${OPT}") already specified"
                ccodeArg="no"
                ;;
            d)
                [[ ${DRYRUN} ]] && die_getopts "-$(lower "${OPT}")|$(upper "${OPT}") already specified"
                DRYRUN="dryrun"
                dryrunSwitch='-d'
                ;;
            g)
                [[ ${githooksArg} ]] && die_getopts "-$(lower "${OPT}")|$(upper "${OPT}") already specified"
                githooksArg="yes"
                ;;
            G)
                [[ ${githooksArg} ]] && die_getopts "-$(lower "${OPT}")|$(upper "${OPT}") already specified"
                githooksArg="no"
                ;;
            h)
                echo "Usage: $0 [-c|C] [-d] [-g|G] [-h] [-l|L] [-m|M] [-r|R] [-s|S]"
                echo "    -c|C    Compile C Code (yes/no).                    [${ccodeDefault}]"
                echo "    -d      Dry-run."
                echo "    -g|G    Make git-hooks (yes/no).                    [${githooksDefault}]"
                echo "    -h      Show the help message."
                echo "    -l|L    Make miniconda symbolic-link (yes/no).      [${minicondaLinkDefault}]"
                echo "    -m|M    Install miniconda (yes/no).                 [${minicondaDefault}]"
                echo "    -r|R    Install development repositories (yes/no).  [${develDefault}]"
                echo "    -s|S    Generate ssh-key (yes/no).                  [${sshkeyDefault}]"
                echo "Use lowercase for yes/true, uppercase for no/false (if applicable)."
                echo "Arguments not set will revert to default settings."
                echo "Packages are installed in the following order:"
                echo "    1  generateSshKey.sh"
                echo "    2  installMiniconda.sh"
                echo "    3  installDevelopment.sh"
                echo "    4  makeGitHooks.sh"
                echo "    5  makeMinicondaLink.sh"
                echo "    6  compileCCode.sh"
                exit
                ;;
            l)
                [[ ${minicondaLinkArg} ]] && die_getopts "-$(lower "${OPT}")|$(upper "${OPT}") already specified"
                minicondaLinkArg="yes"
                ;;
            L)
                [[ ${minicondaLinkArg} ]] && die_getopts "-$(lower "${OPT}")|$(upper "${OPT}") already specified"
                minicondaLinkArg="no"
                ;;
            m)
                [[ ${minicondaArg} ]] && die_getopts "-$(lower "${OPT}")|$(upper "${OPT}") already specified"
                minicondaArg="yes"
                ;;
            M)
                [[ ${minicondaArg} ]] && die_getopts "-$(lower "${OPT}")|$(upper "${OPT}") already specified"
                minicondaArg="no"
                ;;
            r)
                [[ ${develArg} ]] && die_getopts "-$(lower "${OPT}")|$(upper "${OPT}") already specified"
                develArg="yes"
                ;;
            R)
                [[ ${develArg} ]] && die_getopts "-$(lower "${OPT}")|$(upper "${OPT}") already specified"
                develArg="no"
                ;;
            s)
                [[ ${sshkeyArg} ]] && die_getopts "-$(lower "${OPT}")|$(upper "${OPT}") already specified"
                sshkeyArg="yes"
                ;;
            S)
                [[ ${sshkeyArg} ]] && die_getopts "-$(lower "${OPT}")|$(upper "${OPT}") already specified"
                sshkeyArg="no"
                ;;
            *)
                echo "Usage: $0 [-c|C] [-d] [-g|G] [-h] [-l|L] [-m|M] [-r|R] [-s|S]"
                echo "    -h          Show the help message."
                exit
                ;;
        esac
    done
    shift $((OPTIND - 1))
    
    # update with the default-settings
    sshkeyArg="${sshkeyArg:-$sshkeyDefault}"
    minicondaArg="${minicondaArg:-$minicondaDefault}"
    develArg="${develArg:-$develDefault}"
    githooksArg="${githooksArg:-$githooksDefault}"
    minicondaLinkArg="${minicondaLinkArg:-$minicondaLinkDefault}"
    ccodeArg="${ccodeArg:-$ccodeDefault}"

    # dry-run is passed to calls as switch -d
    if [[ ${sshkeyArg} == "yes" ]]; then
        # this would probably be run separately - ignore args and allow input from user
        echo "generateSshKey ${sshkeyArg}"
        ./generateSshKey.sh ${dryrunSwitch}
    fi
    if [[ ${minicondaArg} == "yes" ]]; then
        echo "installMiniconda ${minicondaArg}"
        # use default path
        # ignore symbolic-link
        # answer yes to all
        ./installMiniconda.sh -PLy ${dryrunSwitch}
    fi
    if [[ ${develArg} == "yes" ]]; then
        echo "installDevelopment ${develArg}"
        # clone repositories
        # include existing repositories
        # force create project path
        # use default git-branch
        # ignore git-hooks -> ignore here and use githooksArg
        # use default remote
        # -BKT are to remove user input from generateSshKey script
        # use default ssh-keyfile
        # do not generate new ssh-key
        # do not test ssh-key
        ./installDevelopment.sh -cefGLR -BKT ${dryrunSwitch}
    fi
    if [[ ${githooksArg} == "yes" ]]; then
        echo "makeGitHooks ${githooksArg}"
        ./makeGitHooks.sh ${dryrunSwitch}
    fi
    if [[ ${minicondaLinkArg} == "yes" ]]; then
        echo "makeMinicondaLink ${minicondaLinkArg}"
        # use default conda-path
        ./makeMinicondaLink.sh -P ${dryrunSwitch}
    fi
    if [[ ${ccodeArg} == "yes" ]]; then
        echo "compileCCode ${ccodeArg}"
        # make backup of Makefiles
        # use default conda-path
        ./compileCCode.sh -bC ${dryrunSwitch}
    fi
}

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

print_header "${PROJECT_TITLE} - Install all"
install_all_func "$@"

echo "done"
