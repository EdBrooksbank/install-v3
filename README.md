## Install of CcpNmr AnalysisV3

**This repository contains the scripts for installing the developer's version of CcpNmr AnalysisV3**

More information on AnalysisV3 can be found on the [https://www.ccpn.ac.uk](https://www.ccpn.ac.uk), the repositories are stored on [https://bitbucket.org](https://bitbucket.org),
and the python environment can be found at [https://conda.io](https://conda.io).

---

## Included scripts

There are 6 scripts included in the package:

1. ***generateSshKey.sh***
   Generate an ssh-key to allow access to the developer's repositories on [bitbucket](https://bitbucket.org).
2. ***installMiniconda.sh***
   Install miniconda in the home directory and create the required env.
3. ***installDevelopment.sh***
   Install the AnalysisV3 repositories from the bitbucket.org.
4. ***makeGitHooks.sh***
   Create the internal links between the individual repositories for pre-/post-commit git-hooks.
5. ***compileCCode.sh***
   Compile the required c-code.

and an additional script:

6. ***installAll.sh***
   Run all the above 5 scripts in the correct order. By default, this will install minimising user input.

The installation path for the repositories is defined by PROJECT_DEFAULT in *./projectPath.sh* For Windows, this also needs to be set in *./projectPath.bat*
Once installed, git or pycharm can be used to access other branches, or it can be changed by running the *./installDevelopment.sh* script and only selecting the new git-branch.

---

## Installation

For a first-time install, an ssh-key is required to access the repositories on [bitbucket](https://bitbucket.org).

First run *./generateSshKey.sh*
It will open a website to bitbucket.org from where the user can create/access an account and add the ssh-key.
After a valid ssh-key has been created, this shouldn't need to be called again.

next:

run *./installAll.sh*
Everything will be installed without user input.

The individual scripts can all be run separately as detailed below.

Note:

In order to create the symbolic-links in Windows, run the git-bash shell as administrator.

To allow non-administrators to create symbolic-links:
open the Local Group Policy Editor with *gpedit* from the search-bar;
add your user-group to the *Create symbolic links* option: Computer configuration -> Window Settings -> Security Settings -> User Rights Assignment

To update your change, run *gpupdate /force* from the shell.

---

### *installAll.sh*

There is an overview script that can be used to run all the other scripts.
By default, it assumes that *./generateSshKey.sh* has already been run and skip it, and will then call *./installDevelopment.sh*, *./makeGitHooks.sh*, *./installMiniconda.sh*, and *./compileCCode.sh*

Use with the following options:

`Usage: ./installAll.sh [-c|C] [-d|D] [-g|G] [-h] [-l|L] [-m|M] [-s|S]`


| switch | description                            | default |
|:-------|:---------------------------------------|:--------|
| -c -C  | Compile C Code (yes/no).               | [yes]   |
| -d -D  | Install development (yes/no).          | [yes]   |
| -g -G  | Make git-hooks (yes/no).               | [yes]   |
| -h     | Show the help message.                 |         |
| -l -L  | Make miniconda symbolic-link (yes/no). | [yes]   |
| -m -M  | Install miniconda (yes/no).            | [yes]   |
| -s -S  | Generate ssh-key (yes/no).             | [no]    |

Use lowercase for yes/true, uppercase for no/false (if applicable).
Arguments not set will revert to default settings which can be seen in the square brackets to the right of each option.

The scripts are installed in the following order:

1. ***generateSshKey.sh***
2. ***installMiniconda.sh***
3. ***installDevelopment.sh***
4. ***makeGitHooks.sh***
5. ***makeMinicondaLink.sh***
6. ***compileCCode.sh***

To run without user input:

`./installAll.sh`

To, for example, only install miniconda symbolic-link from here, use:

`./installAll.sh -CDGMS -l`

---

### *generateSshKey.sh*

Generate and test an ssh-key to access the repositories on [bitbucket](https://bitbucket.org).
By default, an ssh-key will be placed in *~/.ssh/ccpn_id_rsa*

Use with the following options:

`Usage: ./generateSshKey.sh [-b <basename>] [-B] [-h] [-k|K] [-t|T] [-y]`


| switch             | description                                                      | default     |
|:-------------------|:-----------------------------------------------------------------|:------------|
| -b&#32;\<basename> | Specify ssh-keyfile basename.                                    |             |
| -B                 | Use default ssh-keyfile, in ${HOME}/.ssh                         | ccpn_id_rsa |
| -h                 | Show the help message.                                           |             |
| -k -K              | Generate ssh-key, this will open the bitbucket website (yes/no). |             |
| -t -T              | Test the ssh-key (yes/no).                                       |             |
| -y                 | Answer 'yes' to prompts that are not explicitly set.             |             |

Use lowercase for yes/true, uppercase for no/false (if applicable).
Arguments not set will request input.

To test an existing key:

`./generateSshKey.sh -BK -t`

---

### *installMiniconda.sh*

Install the miniconda package containing python and dependencies required by AnalysisV3, for more details visit [miniconda](https://conda.io).

Use with the following options:

`Usage: ./installMiniconda.sh [-a|A] [-d|D] [-e|E] [-f] [-h] [-i|I] [-l|L] [-m|M] [-p <path>] [-P] [-r|R] [-y]`


| switch         | description                                                  | default            |
|:---------------|:-------------------------------------------------------------|:-------------------|
| -a -A          | Apply patches (yes/no).                                      |                    |
| -d -D          | Download the latest miniconda (yes/no).                      |                    |
| -e -E          | Install new conda environment (yes/no).                      |                    |
| -f             | Force overwrite of existing miniconda installation.          |                    |
| -h             | Show the help message.                                       |                    |
| -i -I          | Install miniconda package (yes/no).                          |                    |
| -l -L          | Create symbolic link in development folder (yes/no).         |                    |
| -m -M          | Set conda solver to libmamba, quicker than classic (yes/no). |                    |
| -p&#32;\<path> | Specify conda path.                                          |                    |
| -P             | Use default conda path.                                      | ${HOME}/miniconda3 |
| -r -R          | Reset the base conda environment (yes/no).                   |                    |
| -y             | Answer 'yes' to prompts that are not explicitly set.         |                    |

Use lowercase for yes/true, uppercase for no/false (if applicable).
Arguments not set will request input unless -y specified.

To install Miniconda without user input, use:

`./installMiniconda.sh -Py`

---

### *installDevelopment.sh*

Download all the repositories for AnalysisV3 into the folder specified in *./projectPath.sh* and *./projectPath/bat*

Use with the following options:

`Usage: ./installDevelopment.sh [-b|B] [-c|C] [-e|E] [-f] [-g <branch>] [-G] [-p <path>] [-P] [-k|K] [-h] [-r <remote>] [-R] [-t|T]`


| switch             | description                                                      | default     |
|:-------------------|:-----------------------------------------------------------------|:------------|
| -b&#32;\<basename> | Specify ssh-keyfile basename.                                    |             |
| -B                 | Use default ssh-keyfile.                                         | ccpn_id_rsa |
| -c -C              | Clone repositories (yes/no).                                     |             |
| -e -E              | Include existing repositories (yes/no).                          |             |
| -f                 | Force create project path.                                       |             |
| -g&#32;\<branch>   | Specify name for git-branch.                                     |             |
| -G                 | Use default git-branch defined in version.sh.                    | 3.2.0       |
| -h                 | Show the help message.                                           |             |
| -k -K              | Generate ssh-key, this will open the bitbucket website (yes/no). |             |
| -l -L              | Create git-hook links (yes/no).                                  |             |
| -r&#32;\<remote>   | Specify name for remote.                                         |             |
| -R                 | Use default remote.                                              | origin      |
| -t -T              | Test the ssh-key (yes/no).                                       |             |

Use lowercase for yes/true, uppercase for no/false (if applicable).
Arguments not set will request input.

To clone all repositories without user input, run:

`./installDevelopment.sh -cefGlR -BKT`

The project can contain more than one repository, including nested repositories.
These are specified in *projectSettings.sh*.

The git branch for checkout after cloning can be set in *version.sh*.

---

### *makeGitHooks.sh*

Install the symbolic links to connect the pre-/post-commit git-hooks for all the repositories. No individual options are required.
Details about git-hooks can be found at [https://git-scm.com/docs/githooks](https://git-scm.com/docs/githooks).

---

### *compileCCode.sh*

Compile the C code required for the peak-pickers and contour plotting.

Use with the following options:

`Usage: ./compileCCode.sh [-b|B] [-c|C] [-h]`


| switch               | description                                     | default            |
|:---------------------|:------------------------------------------------|:-------------------|
| -b -B                | Backup the Makefiles (yes/no).                  |                    |
| -c&#32;\<conda-path> | Specify the full-path to the conda environment. |                    |
| -C                   | Use default conda path.                         | ${HOME}/miniconda3 |
| -h                   | Show the help message.                          |                    |

Use lowercase for yes/true, uppercase for no/false (if applicable).
Arguments not set will request input.

To run without user input, use:

`./compileCCode.sh -bC`

#### Linux, MacOS

Linux and MacOS are usually trouble free, but may require the installation of OpenGL libraries or *gcc*, there are many ways to do this, please try google :)

#### Windows 10/11

Compilation of the c-code on Windows requires a minimal installation of [https://visualstudio.microsoft.com/vs/community/](https://visualstudio.microsoft.com/vs/community/).
From the installer, go to *Workloads -> Desktop & Mobile -> Desktop development with C++*:
From the right panel, select *C++ core desktop features, MSVC v143 build tools, Windows 11 SDK*

There are slight variations to the names between Windows 10 and 11, choose the ones matching your Visual-Studio and Windows Version.
