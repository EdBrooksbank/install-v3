"""
Module Documentation here
"""

#=========================================================================================
# Licence, Reference and Credits
#=========================================================================================
__copyright__ = "Copyright (C) CCPN project (https://www.ccpn.ac.uk) 2014 - 2024"
__credits__ = ("Ed Brooksbank, Joanna Fox, Morgan Hayward, Victoria A Higman, Luca Mureddu",
               "Eliza Płoskoń, Timothy J Ragan, Brian O Smith, Gary S Thompson & Geerten W Vuister")
__licence__ = ("CCPN licence. See https://ccpn.ac.uk/software/licensing/")
__reference__ = ("Skinner, S.P., Fogh, R.H., Boucher, W., Ragan, T.J., Mureddu, L.G., & Vuister, G.W.",
                 "CcpNmr AnalysisAssign: a flexible platform for integrated NMR analysis",
                 "J.Biomol.Nmr (2016), 66, 111-124, https://doi.org/10.1007/s10858-016-0060-y")
#=========================================================================================
# Last code modification
#=========================================================================================
__modifiedBy__ = "$modifiedBy: Ed Brooksbank $"
__dateModified__ = "$dateModified: 2024-03-19 14:51:28 +0000 (Tue, March 19, 2024) $"
__version__ = "$Revision: 3.2.2 $"
#=========================================================================================
# Created
#=========================================================================================
__author__ = "$Author: Ed Brooksbank $"
__date__ = "$Date: 2024-03-04 15:43:26 +0100 (Mon, March 04, 2024) $"
#=========================================================================================
# Start of code
#=========================================================================================

import os
import sys
import ast
import pathlib
import subprocess
import pkgutil
import warnings
import tabulate
import json
from collections import namedtuple
from operator import or_
from functools import reduce
from importlib.machinery import SourceFileLoader
from importlib.util import find_spec


WORKING_PATH = pathlib.Path().home() / 'Projects/AnalysisV3/src/python/ccpn'
PYTHON_ENV = pathlib.Path(sys.prefix)  # if using active environment
ignore_folders = [WORKING_PATH / 'util/nef/venv',
                  WORKING_PATH / 'macros',
                  WORKING_PATH / 'AnalysisAssign/macros',
                  WORKING_PATH / 'AnalysisStructure/macros',
                  WORKING_PATH / 'AnalysisScreen/macros',
                  WORKING_PATH / 'AnalysisMetabolomics/macros',
                  ]
python_pkg = namedtuple('python_pkg', 'package version build manager')
_PIP_META_TAGS = ['Metadata-Version',  # there may be more, but enough for now
                  'Name',
                  'Version',
                  'Summary',
                  'Home-page',
                  'Download-URL',
                  'Author',
                  'Author-email',
                  'License',
                  'Keywords',
                  'Project-URL',
                  'Platform',
                  'Classifier',
                  'Requires-Python',
                  'License-File',
                  'Requires-Dist',
                  'Provides-Extra',
                  'Description-Content-Type',
                  ]
_COLUMNS = 9


class ConsoleStyle:
    """Colors class:reset all colors with colors.reset; two
    subclasses fg for foreground
    and bg for background; use as colors.subclass.colorname.
    i.e. colors.fg.red or colors.bg.greenalso, the generic bold, disable,
    underline, reverse, strike through,
    and invisible work with the main class i.e. colors.bold
    """
    reset = '\033[0m'
    bold = '\033[01m'
    disable = '\033[02m'
    underline = '\033[04m'
    reverse = '\033[07m'
    strikethrough = '\033[09m'
    invisible = '\033[08m'


    class FG:
        black = '\033[30m'
        darkred = '\033[31m'
        darkgreen = '\033[32m'
        darkyellow = '\033[33m'
        darkblue = '\033[34m'
        darkmagenta = '\033[35m'
        darkcyan = '\033[36m'
        lightgrey = '\033[37m'
        default = '\033[39m'
        darkgrey = '\033[90m'
        red = '\033[91m'
        green = '\033[92m'
        yellow = '\033[93m'
        blue = '\033[94m'
        magenta = '\033[95m'
        cyan = '\033[96m'
        white = '\033[97m'


    class BG:
        black = '\033[40m'
        darkred = '\033[41m'
        darkgreen = '\033[42m'
        darkyellow = '\033[43m'
        darkblue = '\033[44m'
        darkmagenta = '\033[45m'
        darkcyan = '\033[46m'
        lightgrey = '\033[47m'
        default = '\033[49m'
        darkgrey = '\033[100m'
        red = '\033[101m'
        green = '\033[102m'
        yellow = '\033[103m'
        blue = '\033[104m'
        magenta = '\033[105m'
        cyan = '\033[106m'
        white = '\033[107m'


def makeIterableList(inlist=None) -> list:
    """
    Take a nested collection of (tuples, lists or sets) and concatenate into a single list.
    Also changes a single item into a list.
    Removes any Nones from the list.
    :param inlist: list of tuples, lists, sets or single items
    :return: a single list
    """
    # if isinstance(inList, Iterable) and not isinstance(inList, str):
    if isinstance(inlist, (tuple, list, set)):
        return [y for x in inlist for y in makeIterableList(x) if inlist]
    else:
        return [inlist] if inlist is not None else []


def run_command(command):
    """Generate a command-line process
    """
    query = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    status, error = query.communicate()
    if query.poll() == 0:
        return status.decode("utf-8").strip()
    print(status)
    print(error)


def print_table(table, *, header='', long=True, sort=True, colour=ConsoleStyle.FG.white):
    """Print the table as column data, alphabetically running down the columns.
    """
    if not len(table):
        return
    print(f'{colour}', end='')
    if header:
        print(header)
    cols = _COLUMNS if long else _COLUMNS // 2
    if sort:
        table = sorted(list(table))
    while cols > len(table) // 2 and cols > 1:
        cols = cols // 2
    out = tabulate.tabulate([table[x::len(table) // cols] for x in range(len(table) // cols)], tablefmt='plain')
    print('\n'.join(f'    {row}' for row in out.split('\n')))
    print(f'{ConsoleStyle.reset}', end='')  # introduces a newline


def starts_with_single_underscore(string) -> bool:
    """Return True if string starts with a single underscore.
    """
    return string.startswith("_") and not string.startswith("__")


def is_builtin(module_name, builtins=None) -> str:
    """Return True/False if module_name is found as a valid python module.
    If not found then return None.
    """
    # there is weird stuff happening for deprecated module distutils, _distutils_hack unloads it :|
    if (module_spec := find_spec(module_name)) and \
            isinstance(module_spec.loader, SourceFileLoader) and \
            module_spec.loader.path.startswith(str(PYTHON_ENV)) and \
            not starts_with_single_underscore(module_spec.loader.name):
        return module_spec.loader.name if builtins is None or \
                                          (builtins is True and 'site-packages' not in module_spec.loader.path) or \
                                          (builtins is False and 'site-packages' in module_spec.loader.path) else None


def list_all_packages(builtins=None) -> list[str]:
    """List all the available modules.
    Return either all modules, only the built-in modules or only the environment modules.
    """
    return [loader_name
            for _, name, isPkg in pkgutil.iter_modules()
            if (loader_name := is_builtin(name, builtins)) and isPkg
            ]


def get_packages_from_conda() -> list[python_pkg]:
    """Get the list of the modules from the conda environment manager.
    """
    ps = subprocess.run(['conda', 'list'], check=True, capture_output=True)
    processNames = subprocess.run(['tail', '-n', '+4'],
                                  input=ps.stdout, capture_output=True)
    mods = processNames.stdout.decode('utf-8').strip()

    return [python_pkg(*mod.split()) for mod in mods.split('\n')]


def load_file(file: pathlib.Path) -> tuple[str, dict]:
    """Load a json file and return the dict.
    """
    with open(file) as fp:
        data = json.load(fp)
        return file.stem, data


def get_conda_meta_data() -> dict:
    """Get the conda packages from the conda-meta directory.
    """
    files = list_files(PYTHON_ENV / 'conda-meta', extension='json')
    return dict(load_file(fp) for fp in files)


def get_pip_meta_data() -> dict:
    """Get the pip packages from the pkgs directory.
    """
    # get the list of packages in the pip install folder
    dirs = list_dirs(PYTHON_ENV / 'lib' / 'python3.10' / 'site-packages', extension='dist-info', recursive=False)
    meta_dict = {}
    for _dir in dirs:
        try:
            data = pathlib.Path(PYTHON_ENV / 'lib' / 'python3.10' / 'site-packages' / _dir / 'INSTALLER'
                                ).read_text()
            if not data.startswith('pip'):
                # not a pip installed package
                continue
        except FileNotFoundError:
            continue

        # read the meta-data file
        with open(PYTHON_ENV / 'lib' / 'python3.10' / 'site-packages' / _dir / 'METADATA') as fp:
            data = fp.read().split('\n')

        # make a dict from the mate-data tags, I may have missed some
        meta = meta_dict.setdefault(_dir, {})
        for line in data:
            if mtag := next((mtag
                             for mtag in _PIP_META_TAGS
                             if line.startswith(f'{mtag}:')
                             ),
                            None,
                            ):
                dd = meta.setdefault(mtag, None)
                mm = line[len(mtag) + 2:]
                if dd is None:
                    meta[mtag] = mm
                elif isinstance(dd, str):
                    last = [meta[mtag], mm]
                    meta[mtag] = last
                elif isinstance(dd, list):
                    dd.append(mm)

        with open(PYTHON_ENV / 'lib' / 'python3.10' / 'site-packages' / _dir / 'RECORD') as fp:
            meta['files'] = fp.read().split('\n')

    return meta_dict


def get_conda_base():
    """Return the conda-base from the conda environment.
    """
    # this may not be required
    return pathlib.Path(run_command(['conda', 'run', '-n', 'base', 'bash', '-c', 'echo ${CONDA_PREFIX}']))


def get_imports(filename: pathlib.Path) -> list[str]:
    """Read the modules from the package-manager.
    """
    with open(filename, 'r') as file:
        # read the python-file
        try:
            tree = ast.parse(file.read())
        except Exception as es:
            print(filename, es)
            return []

    if filename.name == 'pipeline.py':
        ...  # debugging
    imports = []
    for node in ast.walk(tree):
        if isinstance(node, ast.Import):
            imports.extend(alias.name for alias in node.names)
        elif isinstance(node, ast.ImportFrom):
            imports.append(node.module)
    # list may contain ones for trying to import packages that do not exist
    return list(map(lambda imp: imp.split() and imp.split('.')[0],
                    filter(lambda imp: imp and not imp.startswith('ccpn.') and
                                       imp not in sys.builtin_module_names, imports)))


def get_end_nodes(nested_dict):
    """List the package files and output those that are immediately below site-packages.
    """
    end_nodes = []

    def traverse(node):
        if any(k.startswith('__init__.py') for k in node.keys()):
            return True
        for key, next_node in node.items():
            if traverse(next_node):
                end_nodes.extend(node.keys())
                break

    traverse(nested_dict)
    return end_nodes


def get_modules(pkg_meta):
    """Return the list of python modules from the file description in the package meta-data.
    """
    part_dict = {}
    nodes = []
    if site_packages := list(filter(lambda _fl: '__init__.py' in _fl, pkg_meta.get('files'))):
        for fl in site_packages:
            level = part_dict
            pts = pathlib.Path(fl).parts
            for pt in pts:
                level = level.setdefault(pt, {})
        nodes = get_end_nodes(part_dict)

    # store in the meta-data
    pkg_meta['_modules'] = nodes
    return nodes


def get_all_modules(meta_data: dict) -> list:
    """Return a list of the folders for import as module from the installed conda/pip packages.
    """
    if itms := [set(get_modules(pkg_meta)) for k, pkg_meta in meta_data.items()]:
        return list(reduce(or_, itms))
    return []


# def get_conda_dependencies(pck: python_pkg, conda_base: pathlib.Path):
def get_conda_dependencies(meta_data: dict) -> list:
    """Find the dependencies of the imports from a conda package.
    """

    def deps(pkg: dict):
        return list(map(lambda _pkg: _pkg.split()[0], makeIterableList(pkg.get('depends', []))))

    if itms := [set(deps(pkg_meta)) for k, pkg_meta in meta_data.items()]:
        return list(reduce(or_, itms))
    return []


def get_pip_dependencies(meta_data: dict) -> list:
    """Find the dependencies of the imports from a pip package.
    """

    def deps(pkg: dict):
        return list(map(lambda _pkg: _pkg.split()[0], makeIterableList(pkg.get('Requires-Dist', []))))

    if itms := [set(deps(pkg_meta)) for k, pkg_meta in meta_data.items()]:
        return list(reduce(or_, itms))
    return []


def list_files(folder_path, extension='.py') -> list[pathlib.Path]:
    """List all the files of type 'extension' in a specified folder.
    """
    return [pathlib.Path(root) / file
            for root, dirs, files in os.walk(folder_path)
            if not any(pathlib.Path(root).is_relative_to(igf) for igf in ignore_folders)
            for file in files
            if file.endswith(extension)
            ]


def list_dirs(folder_path, extension='', recursive=True) -> list[pathlib.Path]:
    """List all the directories of type 'extension' in a specified folder.
    """
    return [pathlib.Path(root)
            for root, dirs, files in os.walk(folder_path)
            if not any(root.startswith(igf) for igf in ignore_folders)
            if root.endswith(extension)
            ] if recursive else [pathlib.Path(name)
                                 for name in os.listdir(folder_path)
                                 if os.path.isdir(os.path.join(folder_path, name))
                                 and name.endswith(extension)
                                 ]


def main():
    """
    Print all the conda/pip import information from the specified environment.
    """
    # there is an annoying deprecation warning on loading distutils
    warnings.simplefilter('ignore', UserWarning)

    print("# get the list of python files")
    print(f"# in the working folder -> {WORKING_PATH}")
    print(f"#                          {PYTHON_ENV}")
    folder_path = pathlib.Path(WORKING_PATH)
    files_in_folder = list_files(folder_path)
    fp_names = list({fp.stem for fp in files_in_folder})
    print_table(fp_names, header='PYTHON FILES IN WORKING DIRECTORY', colour=ConsoleStyle.FG.lightgrey, long=False)

    print("# get the list of all imports used in the python files")
    _imports = reduce(or_, _itms) if (_itms := (set(get_imports(fp)) for fp in files_in_folder)) else []

    # get the list of imports that are NOT from files in the working-directory
    #   - these are either builtin- or env- imports
    # imports = list(set(_imports) - set(fp_names))

    print("# get the list of conda packages in this environment")
    print("# from the descriptions in the /conda-meta folder")
    conda_meta = get_conda_meta_data()
    print_table([pck['name'] for pck in conda_meta.values()], header='CONDA-PACKAGES', colour=ConsoleStyle.FG.blue)

    print("# get a list of the python-module imports available from these packages")
    print("#   this also inserts _modules into the meta-data")
    conda_imports = get_all_modules(conda_meta)
    print_table(conda_imports, header='CONDA-PACKAGES --> AVAILABLE CONDA-IMPORTS', colour=ConsoleStyle.FG.cyan)

    print("# list of modules per-package (empty are not listed)")
    for pkg in sorted(conda_meta.values(), key=lambda _pkg: _pkg.get('name')):
        if modules := get_modules(pkg):
            print(pkg.get('name'))
            print_table(modules, colour=ConsoleStyle.FG.cyan)

    print("# get a list of the pip-packages in the environment")
    print("# from the meta-data in the site-packages directory")
    pip_meta = get_pip_meta_data()
    print_table([pck['Name'] for pck in pip_meta.values()], header='PIP-PACKAGES', colour=ConsoleStyle.FG.darkblue)

    print("# get a list of the python-module imports available from these packages")
    print("#   this also inserts _modules into the meta-data")
    pip_imports = get_all_modules(pip_meta)
    print_table(pip_imports, header='PIP-PACKAGES --> AVAILABLE PIP-IMPORTS', colour=ConsoleStyle.FG.darkcyan)

    print("# list of modules per-package (empty are not listed)")
    for pkg in sorted(pip_meta.values(), key=lambda _pkg: _pkg.get('Name')):
        if modules := get_modules(pkg):
            print(pkg.get('Name'))
            print_table(modules, colour=ConsoleStyle.FG.darkcyan)

    print("# get a list of the package-dependencies of the conda-packages")
    conda_dependencies = get_conda_dependencies(conda_meta)
    print_table(conda_dependencies, header='CONDA-PACKAGES --> DEPENDENCIES', colour=ConsoleStyle.FG.blue)

    print("# get a list of the package-dependencies of the pip-packages")
    pip_dependencies = get_pip_dependencies(pip_meta)
    print_table(pip_dependencies, header='PIP-PACKAGES --> DEPENDENCIES', colour=ConsoleStyle.FG.darkblue)

    print("# get the top-level pip-packages that are NOT dependencies")
    print_table(
            list(
                    {pck['name'] for pck in conda_meta.values()}
                    - set(conda_dependencies)
                    ),
            header='CONDA-PACKAGES --> TOP-LEVEL (i.e. -dependencies)',
            colour=ConsoleStyle.FG.green,
            )

    print("# get the top-level pip-packages that are NOT dependencies")
    print_table(
            list(
                    {pck['Name'] for pck in pip_meta.values()} - set(pip_dependencies)
                    ),
            header='PIP-PACKAGES --> TOP-LEVEL (i.e. -dependencies)',
            colour=ConsoleStyle.FG.darkgreen,
            )

    print("# the list of module-imports in the working directory")
    print_table(_imports, header='ALL IMPORTS', colour=ConsoleStyle.FG.magenta)

    print("# from the python-files in the working directory, ")
    print("# list the conda-packages referenced by module-imports")
    import_conda_pkgs = {pkg_meta.get('name') for pkg_meta in conda_meta.values()
                         if set(_imports) & set(pkg_meta.get('_modules'))
                         }
    print_table(import_conda_pkgs, header='ACTIVE CONDA-PACKAGES', colour=ConsoleStyle.FG.darkmagenta)

    print("# from the python-files in the working directory,")
    print("# list the pip-packages referenced by module-imports")
    named_import_pip_pkgs = {pkg_meta.get('Name') for pkg_meta in pip_meta.values()
                             if set(_imports) & set(pkg_meta.get('_modules'))
                             }
    print_table(named_import_pip_pkgs, header='ACTIVE PIP-PACKAGES', colour=ConsoleStyle.FG.darkyellow)
    print()  # clear any console-style control codes


if __name__ == "__main__":
    main()
