#!/usr/bin/env bash
# pull the latest changes for the specified branch
#
#   use default ssh-key ccpn_id_rsa
#   don't clone
#   include all repositories
#   force create project path (as required)
#   use specified git-branch
#   don't generate ssh-key
#   don't create git-hooks
#   use default remote (origin)
#   don't test ssh-key

./installDevelopment.sh -BCef -g 3.2-fred -KLRT

