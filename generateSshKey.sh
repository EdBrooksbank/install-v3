#!/usr/bin/env bash
# install for development environment
# ejb 19/9/17
# updated 17/2/21
#
# Generate an ssh-key to access the git repositories stored on bitbucket.
#
#    Usage: ./generateSshKey.sh [-b <basename>] [-B] [-d] [-h] [-k|K] [-t|T] [-y]
#
#        -b <basename>   Specify ssh-keyfile basename.
#        -B              Use default ssh-keyfile [${HOME}/.ssh/${basenameDefault}].
#        -d              Dry-run.
#        -h              Show the help message.
#        -k|K            Generate ssh-key for git/bitbucket, this will open the bitbucket website (yes/no).
#        -t|T            Test the ssh-key (yes/no).
#        -y              Answer 'yes' to prompts that are not explicitly set.
#
#    Use lowercase for yes/true, uppercase for no/false (if applicable).
#    Arguments not set will request input.
#
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# add a trap to exit script on error 99
trap "exit 99" ERR

# import settings
source ./common.sh
source ./projectSettings.sh

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function generate_ssh_key_func() {
    # process arguments

    # set all to local as may be called from ./installDevelopment
    local basenameArg basenameSource
    local checkGit
    local keygenArg keygen
    local sshKeyfile
    local testKeyArg testKey
    local yesArg
    local OPT OPTIND OPTARG
    local basenameDefault=ccpn_id_rsa

    while getopts ":b:BdhkKtTy" OPT; do
        case ${OPT} in
            b)
                [[ ${basenameArg} ]] && die_getopts "-$(lower "${OPT}")|$(upper "${OPT}") already specified"
                basenameArg=${OPTARG}
                ;;
            B)
                [[ ${basenameArg} ]] && die_getopts "-$(lower "${OPT}")|$(upper "${OPT}") already specified"
                basenameArg=${basenameDefault}
                ;;
            d)
                [[ ${DRYRUN} ]] && die_getopts "-$(lower "${OPT}")|$(upper "${OPT}") already specified"
                DRYRUN="dryrun"
                ;;
            h)
                echo "Usage: $0 [-b <basename>] [-B] [-d] [-h] [-k|K] [-t|T] [-y]"
                echo "    -b <basename>   Specify ssh-keyfile basename."
                echo "    -B              Use default ssh-keyfile [${HOME}/.ssh/${basenameDefault}]."
                echo "    -d              Dry-run."
                echo "    -h              Show the help message."
                echo "    -k|K            Generate ssh-key for git/bitbucket, this will open the bitbucket website (yes/no)."
                echo "    -t|T            Test the ssh-key (yes/no)."
                echo "    -y              Answer 'yes' to prompts that are not explicitly set."
                echo "Use lowercase for yes/true, uppercase for no/false (if applicable)."
                echo "Arguments not set will request input unless -y specified."
                exit
                ;;
            k)
                [[ ${keygenArg} ]] && die_getopts "-$(lower "${OPT}")|$(upper "${OPT}") already specified"
                keygenArg="yes"
                ;;
            K)
                [[ ${keygenArg} ]] && die_getopts "-$(lower "${OPT}")|$(upper "${OPT}") already specified"
                keygenArg="no"
                ;;
            t)
                [[ ${testKeyArg} ]] && die_getopts "-$(lower "${OPT}")|$(upper "${OPT}") already specified"
                testKeyArg="yes"
                ;;
            T)
                [[ ${testKeyArg} ]] && die_getopts "-$(lower "${OPT}")|$(upper "${OPT}") already specified"
                testKeyArg="no"
                ;;
            y)
                [[ ${yesArg} ]] && die_getopts "-${OPT} already specified"
                yesArg="yes"
                ;;
            *)
                echo "Usage: $0 [-b <basename>] [-B] [-d] [-h] [-k|K] [-t|T] [-y]"
                echo "    -h    Show the help message."
                exit
                ;;
        esac
    done
    shift $((OPTIND - 1))

    # ask for inputs at the beginning of script
    keygen=${keygenArg:-$yesArg}
    keygen=${keygen:-$(yesno_prompt "Do you want to create an SSH key?")}

    # create an SSH key for git access
    eval "$(ssh-agent -s)"
    if [[ ${keygen} == "yes" ]]; then

        # check the ssh-key filename first
        basenameSource=${basenameArg:-$(read_default "Please enter basename for ssh-key (in ${HOME}/.ssh/) [${basenameDefault}]:" "${basenameDefault}")}
        sshKeyfile="${HOME}/.ssh/${basenameSource:-$basenameDefault}"
        echo "Output ssh-file: ${sshKeyfile}"

        # generate the ssh-key
        echo "Generating ssh-key..."
        ${DRYRUN} ssh-keygen -t rsa -b 2048 -f "${sshKeyfile}"
        echo "...a website should have opened to https://${BITBUCKET_WEBSITE}"
        echo "login; navigate to 'Personal settings -> SSH Keys'"
        echo "and click 'Add Key'"
        echo "Paste the whole of the following line into the box labelled 'Key*':"
        echo
        ${DRYRUN} cat "${sshKeyfile}.pub"
        echo
        echo "If you have any issues, manually copy the contents of ${sshKeyfile}.pub"

        if is_windows; then
            cmd <<< "explorer \"https://${BITBUCKET_WEBSITE}\"" > /dev/null
        else
            open "https://${BITBUCKET_WEBSITE}"
        fi
        space_continue

    else
        basenameSource=${basenameArg:-$basenameDefault}
        sshKeyfile="${HOME}/.ssh/${basenameSource:-$basenameDefault}"
        echo "Output ssh-file: ${sshKeyfile}"
    fi

    # make sure that bitbucket.org is added to the config file
    if [[ ! -f "${HOME}/.ssh/config" || $(grep bitbucket.org "${HOME}/.ssh/config") == '' ]]; then
        ${DRYRUN} echo "Host ${BITBUCKET_WEBSITE}
  PreferredAuthentications publickey
  IdentityFile ${sshKeyfile}" >> "${HOME}/.ssh/config"
    fi
    ${DRYRUN} ssh-add "${sshKeyfile}"
    # required for rocky9.0, maybe others
    ${DRYRUN} chmod 600 "${HOME}/.ssh/config"

    # test the ssh-key
    testKey=${testKeyArg:-$yesArg}
    testKey=${testKey:-$(yesno_prompt "Do you want to test ssh-key (${HOME}/.ssh/${basenameSource})?")}

    if [[ ${testKey} == "yes" ]]; then
        echo "Testing ssh-key..."
        checkGit=$(${DRYRUN} ssh -T git@${BITBUCKET_WEBSITE})
        echo "${checkGit}"
        if [[ ${checkGit} == *"logged in as"* || ${checkGit} == *"authenticated"* ]]; then
            echo "ssh-key okay"
        else
            echo "ssh-key not working"
            exit
        fi
    fi
}

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

print_header "${PROJECT_TITLE} - Generate ssh-key"
generate_ssh_key_func "$@"

echo "done"

# try these:
#
# permissions
#    $ chmod 700 ~/.ssh
#    $ chmod 400 ~/.ssh/ccpn_id_rsa
#    $ chmod 400 ~/.ssh/ccpn_id_rsa.pub
#
#Either
#
#    specify which key to use
#    $ ssh -i ~/.ssh/ccpn_id_rsa user@host
#
#    add to ~/.ssh/config:
#
#    Host bitbucket.org
#        PreferredAuthentications publickey
#        IdentityFile ~/.ssh/another_private_key <- just using these lines
#
#OR
#
#    add to ~/.ssh/config:
#
#    Host bitbucket.org
#      HostkeyAlgorithms +ssh-rsa
#      PubkeyAcceptedAlgorithms +ssh-rsa <- this gives an error
