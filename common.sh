#!/usr/bin/env bash

# add a trap to exit script on error 99
trap "exit 99" ERR

# Operating system list
OS_LIST=(Linux MacOS Windows)
OS_BUILDS=(Linux MacOS MacOSM Win10 Win11)

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# available functions

function git_repository() {
    # return the current git_repository
    gitBranch="$(git rev-parse --abbrev-ref HEAD)"
    echo "${gitBranch}"
}

function check_git_repository() {
    # check that the current path contains the correct branch
    gitBranch="$(git rev-parse --abbrev-ref HEAD)"
    thisPath="$(pwd)"
    if [[ ${gitBranch} == "${GIT_RELEASE}" ]]; then
        echo "correct git branch on path: ${thisPath}"
    else
        echo "*** Not correct branch ***: Current git repository ${gitBranch} - expected ${GIT_RELEASE}"
        exit 99
    fi
}

function local_git_exists() {
    # check whether the local git branch exists
    localExists="$(git branch --list "$1")"
    if [[ -z "${localExists}" ]]; then
        echo 0
    else
        echo 1
    fi
}

function continue_prompt() {
    # prompt for a yes/no answer - defaults to yes on enter
    # answering no will terminate
    # input $1 can still contain trailing question mark for clarity
    local prompt yn prompt_postfix
    if [[ $1 == *"?" ]]; then
        # remove the question mark from the end
        prompt="${1:0:${#1}-1}"
    else
        prompt=$1
    fi
    prompt_postfix="[Y]/n"
    while true; do
        read -rp "${prompt} ${prompt_postfix}?" yn >&2
        yn="${yn:-yes}"
        case ${yn} in
            [Yy]*)
                break
                ;;
            [Nn]*)
                exit 99
                ;;
            *) echo "Please answer ${prompt_postfix}" >&2 ;;
        esac
    done
}

function yesno_prompt() {
    # prompt for a yes/no answer - defaults to yes on enter
    # program flow will continue
    # input $1 can still contain trailing question mark for clarity
    # $2 optional = yes|Yes|y|Y|no|No|n|N - unspecified defaults to yes
    local prompt yn default prompt_postfix
    if [[ $1 == *"?" ]]; then
        # remove the question mark from the end
        prompt="${1:0:${#1}-1}"
    else
        prompt=$1
    fi
    default=${2:-yes}
    case ${default} in
        [Yy]*) prompt_postfix="[Y]/n" ;;
        *) prompt_postfix="y/[N]" ;;
    esac
    while true; do
        read -rp "${prompt} ${prompt_postfix}?" yn >&2
        yn="${yn:-$default}"
        case ${yn} in
            [Yy]*)
                echo "yes"
                break
                ;;
            [Nn]*)
                echo "no"
                break
                ;;
            *) echo "Please answer ${prompt_postfix}" >&2 ;;
        esac
    done
}

function yesno_prompt_required() {
    # prompt for a yes/no answer, and return yes or no, input is required
    # (may be to similar to yesno_prompt)
    # input $1 can still contain trailing question mark for clarity
    if [[ "$1" == *"?" ]]; then
        # remove the question mark from the end
        prompt="${1:0:${#1}-1}"
    else
        prompt=$1
    fi
    while true; do
        read -rp "${prompt} [Y/N]?" yn >&2
        case ${yn} in
            [Yy]*)
                echo "yes"
                break
                ;;
            [Nn]*)
                echo "no"
                break
                ;;
            *)
                echo "Please answer [Y/N]" >&2
                ;;
        esac
    done
}

function get_machine_type() {
    # detect the current OS type
    # return a user-friendly version of the machine type
    local type
    case "$(uname)" in
        Linux*)
            type=Linux
            ;;
        Darwin*)
            type=MacOS
            ;;
        *NT*)
            type=Windows
            ;;
        *)
            type="UNKNOWN"
            ;;
    esac
    echo "${type}"
}

function detect_os() {
    # detect the current OS type
    # return a code-friendly version of the os
    local machine
    case "$(uname)" in
        Linux*)
            machine=linux
            ;;
        Darwin*)
            machine=osx
            ;;
        *NT*)
            machine=win
            ;;
        *)
            machine="UNKNOWN"
            ;;
    esac
    echo "${machine}"
}

function detect_arch() {
    # detect the cpu-type
    local arch
    arch=$(get_bit_count)
    case "${arch}" in
        aarch64 | ppc64le | arm64) ;;
            # pass
        *)
            arch="64"
            ;;
    esac
    echo "${arch}"
}

function valid_os() {
    local os
    os="$(detect_os)-$(detect_arch)"
    case "${os}" in
        linux-aarch64 | linux-ppc64le | linux-64 | osx-arm64 | osx-64 | win-64)
            echo "${os}"
            ;;
        *)
            echo "Not a valid OS" >&2
            ;;
    esac
}

function show_choices() {
    # show OS choices in a table
    # pass in a string and an [*] array
    local title
    local chars
    local divider
    local index
    title="${1}"
    chars=${#title}
    divider=$(head -c "${chars}" < /dev/zero | tr "\0" "~")
    echo "${title}" >&2
    echo "${divider}" >&2
    index=1
    for opt in ${2}; do
        echo "  ${index}. ${opt}" >&2
        index=$((index + 1))
    done
    echo "  ${index}. exit" >&2
}

function get_machine() {
    local choice
    local mch
    mch="$(detect_os)"
    if [[ ${mch} != *"UNKNOWN"* ]]; then
        echo "${mch}"
    else
        if is_windows; then
            echo "win"
        else
            echo "local OS not in [${OS_LIST[*]}]: ${mch}" >&2
            continue_prompt "do you want to try an OS from the list?" || exit $?
            select_machine
        fi
    fi
}

function select_machine() {
    local choice
    show_choices "Select target OS" "${OS_LIST[*]}"
    choice=$(read_choice ${#OS_LIST[@]} " select target OS from the list > ") || exit $?
    echo "${OS_LIST[$((choice - 1))]}"
}

function select_build() {
    local choice
    show_choices "Select OS-build" "${OS_BUILDS[*]}"
    choice=$(read_choice ${#OS_BUILDS[@]} " select OS-build from the list > ") || exit $?
    echo "${OS_BUILDS[$((choice - 1))]}"
}

function distro() {
    echo "$(linux_name)$(linux_version_id)"
}

function os_name() {
    # get the current name from the os-release file
    local name
    if is_linux; then
        name=$(is_linux && [[ -f /etc/os-release ]] && grep "^ID=" /etc/os-release)
        name=$(remove_prefix "${name}" "ID=")
        name=$(strip_quotes "${name}")
        name=${name%%[^a-zA-Z0-9]*}
        echo "${name^}"
    elif is_macos; then
        # Put the Os name here - e.g., Catalina
        ver=$(sw_vers -productVersion)
        # case here
        case "${ver}" in
            10.12*) echo "Sierra";;
            10.13*) echo "HighSierra";;
            10.14*) echo "Mojave";;
            10.15*) echo "Catalina";;
            11*) echo "BigSur";;
            12*) echo "Monterey";;
            13*) echo "Ventura";;
            14*) echo "Sonoma";;
            *) echo "Undefined MacOS release name" >&2;;
        esac
    else
        detect_os
    fi
}

function os_version() {
    # get the current version from the os-release file
    local version
    if is_linux; then
        version=$([[ -f /etc/os-release ]] && grep "^VERSION_ID=" /etc/os-release)
        version=$(remove_prefix "${version}" "VERSION_ID=")
        strip_quotes "${version}"
    elif is_macos; then
        sw_vers -productVersion
    elif is_windows; then
        ver=$(uname -s | grep -Eo "[0-9]+" | tail -1)
        if [[ ${ver} -gt 20000 ]]; then
            echo "11"
        else
            echo "10"
        fi
    fi
}

function merge_conda_pip() {
    # return information from the python-distro package
    py="import sys
conda_in=sys.argv[1:] or ['']
conda_pcks=[ll for pck in conda_in[0].split('\n')
           for ll in pck.split()[:1]]
# read from the pipe
fp = sys.stdin.read().split('\n')
for pck in fp:
    if pck and pck.split() and len(pck.split()) == 2:
        name, ver = pck.split()[:2]
        if name.lower() not in conda_pcks:
            print('  -', '=='.join([name, ver]))"
    if command_exists python; then
        python -c "${py}" "$@"
    elif command_exists python3; then
        python3 -c "${py}" "$@"
    else
        echo "Error: merge_conda_pip - no python" >&2
        exit
    fi
}

function get_build() {
    if is_windows; then
        ver=$(uname -s | grep -Eo "[0-9]+" | tail -1)
        if [[ ${ver} -gt 20000 ]]; then
            echo "win11"
        else
            echo "win10"
        fi
    elif is_macos; then
        valid_os
    elif is_linux; then
        distro
    fi
}

function get_machine_append() {
    # set the machine extension for MacOS
    if is_macos; then
        # required for getting the correct path from miniconda website
        echo "X"
    fi
}

function get_bit_count() {
    # set the bit-count for the machine
    uname -m
}

function get_digit() {
    # read a digit from the user, until between 1 and n
    while true; do
        read -rsn1 num >&2
        case ${num} in
            [0123456789]*)
                echo "${num}"
                break
                ;;
            *) ;;
        esac
    done
}

function read_choice() {
    # read a choice from the user
    local choice=0
    while true; do
        read -rp "$2" choice >&2
        if [[ $((choice)) != "${choice}" ]]; then
            echo "not a number" >&2
        else
            if [[ ${choice} == $(($1 + 1)) ]]; then
                exit 99
            fi
            if [[ ${choice} -ge 1 && ${choice} -le $1 ]]; then
                break
            fi
        fi
    done
    echo "${choice}"
}

function strip() {
    # strip leading/trailing whitespace
    echo "$1" | sed -e 's/^[ \t]*//' | sed -e 's/[ \t]*$//'
}

function strip_quotes() {
    # strip leading/trailing quotes/whitespace
    echo "$1" | sed -e 's/^[ \t'"'"'"]*//' | sed -e 's/[ \t'"'"'"]*$//'
}

function remove_prefix() {
    # remove prefix from string
    # $1 string
    # $2 prefix to remove
    echo "${1//$2/}"
}

function read_valid_path() {
    # read a valid path from the user
    # $1 is the statement
    # $2 is an optional default
    local choice count=3 question
    question=$(strip "$1")
    while [[ ${count} -gt 0 ]]; do
        count=$((count - 1))
        read -rp "${question} " choice >&2
        choice=${choice:-$2}
        if [[ -d "${choice}" ]]; then
            echo "${choice}"
            break
        fi
        if [[ ${count} == 0 ]]; then
            echo "Error - too many tries" >&2
            exit 99
        fi
        echo "Error - path not found or not a directory" >&2
    done
}

function read_default() {
    # read an input from the user
    # $1 is the statement
    # $2 is an optional default for return
    local statement choice
    statement=$(strip "$1")
    read -rp "${statement} " choice >&2
    choice=${choice:-$2}
    echo "${choice}"
}

function space_continue() {
    # wait for space bar
    read -n1 -rp "press space to continue..."
    echo ""
}

function relative_path() {
    # return the relative path to the current path using python script
    # bit of a cheat using python
    if command_exists python3; then
        python3 -c "import os,sys;print (os.path.relpath(*(sys.argv[1:])))" "$@"
    elif command_exists python; then
        python -c "import os,sys;print (os.path.relpath(*(sys.argv[1:])))" "$@"
    else
        echo "Error: relative_path - cannot find python3/python"
        exit
    fi
}

function windows_path() {
    # return the current path - translates to machine specific path
    # bit of a cheat using python
    if command_exists python3; then
        python3 -c "import os,sys;print (os.path.abspath(*(sys.argv[1:])))" "$@"
    elif command_exists python; then
        python -c "import os,sys;print (os.path.abspath(*(sys.argv[1:])))" "$@"
    else
        echo "Error: windows_path - cannot find python3/python"
        exit
    fi
}

function command_exists() {
    # check whether the given command exists
    command -v "$1" > /dev/null 2>&1
}

function die_getopts() {
    # print error string and quit
    echo "Error: $*." >&2
    exit 1
}

function is_dir_in_path() {
    # check whether the PATH contains a given directory
    case ":${PATH}:" in
        *:"$1":*) return 0 ;;
        *) return 1 ;;
    esac
}

function check_item_in_list() {
    # Check whether the first argument is contained in the following list
    local item
    local msg="$1"   # Save first argument in a variable
    shift            # Shift all arguments to the left (original $1 gets lost)
    local arr=("$@") # Get the remaining arguments as a set
    for item in "${arr[@]}"; do
        [[ ${item} == "${msg}" ]] && echo "true"
        return
    done
}

function is_windows() {
    # check whether windows
    [[ -n "${WINDIR}" ]]
}

function is_macos() {
    # check whether MacOS
    [[ $(uname -s) == "Darwin" ]]
}

function is_linux() {
    # check whether Linux
    [[ $(uname -s) == "Linux" ]]
}

function make_link() {
    # Cross-platform symlink function. With one parameter, it will check
    # whether the parameter is a symlink. With two parameters, it will create
    # a symlink to a file or directory.
    # Usage: make_link $target $link
    # target: file to link to - same ordering as linux
    # link: new link to be created
    target=$1
    link=$2
    if [[ -z "${link}" ]]; then
        # Link-checking mode; test the target
        if is_windows; then
            targetPath=$(windows_path "${target}")
            cmd <<< "fsutil reparsepoint query \"${targetPath}\"" > /dev/null
        else
            [[ -L "${target}" ]]
        fi
    else
        # Link-creation mode.
        if [[ ! -e "${link}" && -e "${target}" ]]; then
            if is_windows; then
                # Windows needs to be told if it's a directory or not. Infer that.
                targetPath=$(windows_path "${target}")
                linkPath=$(windows_path "${link}")
                # windows is reversed order
                if [[ -d "${target}" ]]; then
                    cmd <<< "mklink /D \"${linkPath}\" \"${targetPath}\"" > /dev/null
                else
                    cmd <<< "mklink \"${linkPath}\" \"${targetPath}\"" > /dev/null
                fi
            else
                # linux parameters the other way around
                ln -s "${target}" "${link}"
            fi
        fi
    fi
}

function remove_link() {
    # Remove a link, cross-platform.
    # Usage: remove_link $target
    # target: link/file to remove
    target=$1
    if [[ -L "${target}" ]]; then
        if is_windows; then
            # Again, Windows needs to be told if it's a file or directory.
            # Use python Path function
            # check - does the source exist?
            targetPath=$(windows_path "${target}")
            if [[ -d "${target}" ]]; then
                cmd <<< "rmdir \"${targetPath}\"" > /dev/null
            else
                cmd <<< "del /f \"${targetPath}\"" > /dev/null
            fi
        else
            rm -r "${target}"
        fi
    fi
}

# rename a directory, cross-platform
function rename_directory() {
    # Usage: rename_directory $target $newName
    # target: directory to rename
    # newName: new name
    target=$1
    newName=$2
    if [[ -e "${target}" ]]; then
        if is_windows; then
            # Again, Windows needs to be told if it's a file or directory.
            # Use python Path function - although mv seems to work in git shell
            targetPath=$(windows_path "${target}")
            if [[ -d "${target}" ]]; then
                cmd <<< "rename \"${targetPath}\" \"${newName}\"" > /dev/null
            fi
        else
            mv -v "${target}" "${newName}"
        fi
    fi
}

function lower() {
    echo "$1" | tr '[:upper:]' '[:lower:]'
}

function upper() {
    echo "$1" | tr '[:lower:]' '[:upper:]'
}

function script_path() {
    # return the path of the script
    pushd . > /dev/null
    local script_path
    script_path="${BASH_SOURCE[0]}"
    while [[ -L "${script_path}" ]]; do
        cd "$(dirname "${script_path}")" || exit
        script_path="$(readlink "$(basename "${script_path}")")"
    done
    cd "$(dirname "${script_path}")" > /dev/null || exit
    script_path="$(pwd)"
    popd > /dev/null || exit
    echo "${script_path}"
}

function to_binary() {
    local n bits sign=''
    (($1 < 0)) && sign=-
    for ((n = ${sign}$1; n > 0; n >>= 1)); do
        bits=$((n & 1))${bits}
    done
    printf "%s\n" "${sign}${bits-0}"
}

function print_header() {
    # make a heading
    local title chars=0 divider
    if [ ${SHLVL} -lt 3 ]; then
        # only display the heading if called from the commandline
        #   will show if called with 'source'
        for word in "$@"; do
            if [ ${#word} -gt ${chars} ]; then
                chars=${#word}
            fi
        done
        divider=$(head -c "${chars}" < /dev/zero | tr "\0" "~")

        echo "${divider}"
        for word in "$@"; do
            echo "${word}"
        done
        echo "${divider}"
    fi
}

function dryrun() {
    # prefix an instruction with this to change to a dry-run echo
    local string
    for word in "$@"; do
        string="${string} ${word}"
    done
    echo "dry-run $(strip "${string}")"
}

function dryrun_pipe() {
    # NOT TESTED
    cat && dryrun "$1"
}
