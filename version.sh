#!/usr/bin/env bash

# version
export GIT_RELEASE="3.2.2.1"
export RELEASE_VERSION="3.2.2.1"

# miniconda source
export CONDA_SOURCE="ccpn-v3.2.0"
