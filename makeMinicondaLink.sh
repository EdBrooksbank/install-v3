#!/usr/bin/env bash
# Install miniconda and apply patches
#
# Make a symbolic-link between the env in the miniconda package
# and the miniconda folder in AnalysisV3
#
#    Usage: ./makeMinicondaLink.sh [-d] [-p <path>] [-P]
#
#        -d          Dry-run.
#        -p <path>   Specify conda path.
#        -P          Use default conda path [condaDefault].
#
#    Use lowercase for yes/true, uppercase for no/false (if applicable).
#
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# add a trap to exit script on error 99
trap "exit 99" ERR

# import settings
source ./common.sh
source ./projectSettings.sh

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function make_symbolic_link_func() {
    # process arguments

    # set all to local as may be sourced
    local condaPathArg condaDefault condaPath condaEnvPath

    condaDefault="${HOME}/${DEFAULT_CONDA}"
    while getopts ":dhp:P" OPT; do
        case ${OPT} in
            d)
                [[ ${DRYRUN} ]] && die_getopts "-$(lower "${OPT}")|$(upper "${OPT}") already specified"
                DRYRUN="dryrun"
                ;;
            h)
                echo "Usage: $0 [-p <path>] [-P]"
                echo "    -d          Dry-run."
                echo "    -p <path>   Specify conda path."
                echo "    -P          Use default conda path [${condaDefault}]."
                echo "Use lowercase for yes/true, uppercase for no/false (if applicable)."
                exit
                ;;
            p)
                [[ ${condaPathArg} ]] && die_getopts "-$(lower "${OPT}")|$(upper "${OPT}") already specified"
                condaPathArg=${OPTARG}
                ;;
            P)
                [[ ${condaPathArg} ]] && die_getopts "-$(lower "${OPT}")|$(upper "${OPT}") already specified"
                condaPathArg=${condaDefault}
                ;;
            *)
                echo "Usage: $0 [-p <path>] [-P]"
                echo "    -h          Show the help message."
                exit
                ;;
        esac
    done
    shift $((OPTIND - 1))

    # specify the conda path
    condaPath=${condaPathArg:-$(read_valid_path "Please enter miniconda installation path [${condaDefault}]:" "${condaDefault}")}
    condaEnvPath="${condaPath}/envs/${CONDA_SOURCE}"
    if [[ ! -d "${condaEnvPath}" ]]; then
        echo "Error - env path not found or not a directory"
        exit
    fi

    # execute that shell script to make sure the paths are activated
    if [[ -f "${HOME}/.bash_profile" ]]; then
        echo "executing ~/.bash_profile"
        source "${HOME}/.bash_profile" || exit
    fi

    # creating link
    echo "path to [${PROJECT_DEFAULT}/]${ENV_PATH}: ${condaEnvPath}"
    cd "${PROJECT_DEFAULT}" || exit
    if [[ ! -d "${condaEnvPath}" ]]; then
        echo "Error compiling - conda environment ${CONDA_SOURCE} does not exist"
        exit
    fi
    # create symbolic link
    echo "creating symbolic-link ${condaEnvPath} -> ${PROJECT_DEFAULT}/${ENV_PATH}"
    ${DRYRUN} remove_link "${ENV_PATH}"
    ${DRYRUN} make_link "${condaEnvPath}" "${ENV_PATH}"
}

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

print_header "${PROJECT_TITLE} - Make symbolic-link to ${ENV_PATH}"
make_symbolic_link_func "$@"

echo "done"
