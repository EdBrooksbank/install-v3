#!/usr/bin/env bash
# install for development environment
# ejb 19/9/17
# updated 17/2/21
#
# Download all repositories for Project.
#
#    Usage: ./installDevelopment.sh [-b|B] [-c|C] [-d] [-e|E] [-f] [-g <branch>] [-G] [-h] [-k|K] [-l|L] [-r <remote>] [-R] [-t|T]
#
#        -b <basename>   Specify ssh-keyfile basename.
#        -B              Use default ssh-keyfile [${HOME}/.ssh/${BASENAME_DEFAULT}].
#        -c|C            Clone repositories (yes/no).
#        -d              Dry-run.
#        -e|E            Include existing repositories (yes/no).
#        -f              Force create project path.
#        -g <branch>     Specify name for git-branch.
#        -G              Use default git-branch defined in version.sh [${GIT_RELEASE}].
#        -k|K            Generate ssh-key for git/bitbucket, this will open the bitbucket website (yes/no).
#        -l|L            Create git-hook links (yes/no).
#        -h              Show the help message.
#        -r <remote>     Specify name for remote.
#        -R              Use default remote [${REMOTE_DEFAULT}].
#        -t|T            Test the ssh-key (yes/no).
#
#    Use lowercase for yes/true, uppercase for no/false (if applicable).
#    Arguments not set will request input.
#
#    To clone all repositories without user input:
#
#        ./installDevelopment.sh -cefGlR -BKT
#
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# add a trap to exit script on error 99
trap "exit 99" ERR

# import settings
source ./common.sh
source ./projectSettings.sh

REMOTE_DEFAULT=origin
BASENAME_DEFAULT=ccpn_id_rsa
GIT_DEFAULT=master

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# make a heading
print_header "${PROJECT_TITLE} - Install development"

# process arguments
while getopts ":b:BcCdeEfg:GhkKlLr:RtT" OPT; do
    case ${OPT} in
        b)
            [[ ${basenameArg} ]] && die_getopts "-$(lower "${OPT}")|$(upper "${OPT}") already specified"
            basenameArg=${OPTARG}
            # build args to pass to generateSshKey.sh
            sshSwitches="${sshSwitches} -${OPT} ${OPTARG}"
            ;;
        B)
            [[ ${basenameArg} ]] && die_getopts "-$(lower "${OPT}")|$(upper "${OPT}") already specified"
            basenameArg=${BASENAME_DEFAULT}
            sshSwitches="${sshSwitches} -${OPT}"
            ;;
        c)
            [[ ${cloneArg} ]] && die_getopts "-$(lower "${OPT}")|$(upper "${OPT}") already specified"
            cloneArg="yes"
            ;;
        C)
            [[ ${cloneArg} ]] && die_getopts "-$(lower "${OPT}")|$(upper "${OPT}") already specified"
            cloneArg="no"
            ;;
        d)
            [[ ${DRYRUN} ]] && die_getopts "-$(lower "${OPT}")|$(upper "${OPT}") already specified"
            DRYRUN="dryrun"
            sshSwitches="${sshSwitches} -${OPT}"
            githookSwitches="-d"
            ;;
        e)
            [[ ${existingArg} ]] && die_getopts "-$(lower "${OPT}")|$(upper "${OPT}") already specified"
            existingArg="yes"
            ;;
        E)
            [[ ${existingArg} ]] && die_getopts "-$(lower "${OPT}")|$(upper "${OPT}") already specified"
            existingArg="no"
            ;;
        f)
            [[ ${forceArg} ]] && die_getopts "-${OPT} already specified"
            forceArg="yes"
            ;;
        g)
            [[ ${gitBranchArg} ]] && die_getopts "-$(lower "${OPT}")|$(upper "${OPT}") already specified"
            gitBranchArg="yes"
            gitBranch=${OPTARG}
            ;;
        G)
            [[ ${gitBranchArg} ]] && die_getopts "-$(lower "${OPT}")|$(upper "${OPT}") already specified"
            gitBranchArg="no"
            gitBranch=${GIT_RELEASE}
            ;;
        h)
            echo "Usage: $0 [-b|B] [-c|C] [-d] [-e|E] [-f] [-g <branch>] [-G] [-h] [-k|K] [-l|L] [-r <remote>] [-R] [-t|T]"
            echo "    -b <basename>   Specify ssh-keyfile basename."
            echo "    -B              Use default ssh-keyfile [${HOME}/.ssh/${BASENAME_DEFAULT}]."
            echo "    -c|C            Clone repositories (yes/no)."
            echo "    -d              Dry-run."
            echo "    -e|E            Include existing repositories (yes/no)."
            echo "    -f              Force create project path."
            echo "    -g <branch>     Specify name for git-branch."
            echo "    -G              Use default git-branch defined in version.sh [${GIT_RELEASE}]."
            echo "    -h              Show the help message."
            echo "    -k|K            Generate ssh-key for git/bitbucket, this will open the bitbucket website (yes/no)."
            echo "    -l|L            Create git-hook links (yes/no)."
            echo "    -r <remote>     Specify name for remote."
            echo "    -R              Use default remote [${REMOTE_DEFAULT}]."
            echo "    -t|T            Test the ssh-key (yes/no)."
            echo "Use lowercase for yes/true, uppercase for no/false (if applicable)."
            echo "Arguments not set will request input."
            echo "To clone all repositories without user input:"
            echo "$0 -cefGlR -BKT"
            exit
            ;;
        k)
            [[ ${keygenArg} ]] && die_getopts "-$(lower "${OPT}")|$(upper "${OPT}") already specified"
            keygenArg="yes"
            sshSwitches="${sshSwitches} -${OPT}"
            ;;
        K)
            [[ ${keygenArg} ]] && die_getopts "-$(lower "${OPT}")|$(upper "${OPT}") already specified"
            keygenArg="no"
            sshSwitches="${sshSwitches} -${OPT}"
            ;;
        l)
            [[ ${githooksArg} ]] && die_getopts "-$(lower "${OPT}")|$(upper "${OPT}") already specified"
            githooksArg="yes"
            ;;
        L)
            [[ ${githooksArg} ]] && die_getopts "-$(lower "${OPT}")|$(upper "${OPT}") already specified"
            githooksArg="no"
            ;;
        r)
            [[ ${remoteArg} ]] && die_getopts "-$(lower "${OPT}")|$(upper "${OPT}") already specified"
            remoteArg=${OPTARG}
            ;;
        R)
            [[ ${remoteArg} ]] && die_getopts "-$(lower "${OPT}")|$(upper "${OPT}") already specified"
            remoteArg=${REMOTE_DEFAULT}
            ;;
        t)
            [[ ${testKeyArg} ]] && die_getopts "-$(lower "${OPT}")|$(upper "${OPT}") already specified"
            testKeyArg="yes"
            sshSwitches="${sshSwitches} -${OPT}"
            ;;
        T)
            [[ ${testKeyArg} ]] && die_getopts "-$(lower "${OPT}")|$(upper "${OPT}") already specified"
            testKeyArg="no"
            sshSwitches="${sshSwitches}${OPT}"
            ;;
        *)
            echo "Usage: $0 [-b|B] [-c|C] [-d] [-e|E] [-f] [-g <branch>] [-G] [-h] [-k|K] [-l|L] [-r <remote>] [-R] [-t|T]"
            echo "    -h    Show the help message."
            exit
            ;;
    esac
done
shift $((OPTIND - 1))

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# get the current script path
script_path=$(pwd -P)

# initialise parent path
if [[ -d "${PROJECT_DEFAULT}" ]]; then
    if [[ ${forceArg} != "yes" ]]; then
        continue_prompt "Directory ${PROJECT_DEFAULT} exists, do you want to continue?"
    else
        echo "${PROJECT_DEFAULT} exists"
    fi
fi

# set remote
remoteSource=${remoteArg:-$(read_default "Please enter name for remote [${REMOTE_DEFAULT}]:" "${REMOTE_DEFAULT}")}
remoteSource="${remoteSource:-$REMOTE_DEFAULT}"
echo "Use remote: ${remoteSource}"

# check whether using a Mac
export_osx_dyld_path

# ask for inputs at the beginning of script
clone=${cloneArg:-$yesArg}
[[ ${clone} ]] && echo "Clone repositories: ${clone}"
clone=${clone:-$(yesno_prompt "Do you want to clone repositories?")}

# include existing repositories
existing=${existingArg:-$yesArg}
[[ ${existing} ]] && echo "Include existing repositories: ${existing}"
existing=${existing:-$(yesno_prompt "Do you want to include existing repositories?")}

# create git-hook links in each repository
githooks=${githooksArg:-$yesArg}
[[ ${githooks} ]] && echo "Create git-hook links: ${githooks}"
githooks=${githooks:-$(yesno_prompt "Do you want to create git-hook links?")}

# checkout git branch
if [[ ! ${gitBranchArg} ]]; then
    gitCheckout=$(yesno_prompt "Do you want to checkout new git-branch?")
    if [[ ${gitCheckout} == "yes" ]]; then
        echo "MAKE SURE THERE ARE NO STASHES/COMMITS PENDING"
        gitBranch=$(read_default "Please enter branch to checkout [${GIT_RELEASE}]:" "${GIT_RELEASE}")
        gitBranch=$(strip_quotes "${gitBranch}")
    fi
fi
[[ ${gitBranch} ]] && echo "Use git-branch: ${gitBranch}"

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# ask to create an ssh-key - dry-run passes a switch
source ./generateSshKey.sh ${sshSwitches} || exit  # doesn't want to be quoted
# testing the ssh-add, required if not using 'source' as above
#sshKeyfile=$(ssh -G "${BITBUCKET_WEBSITE}" | grep identityfile | awk '{print $2}' | head -n 1)
#${DRYRUN} eval "$(ssh-agent -s)"
#${DRYRUN} ssh-add "${sshKeyfile}"

# need to download/clone and set up repositories here
if [[ ${clone} == "yes" ]]; then
    echo "Clone repositories"
    echo "${REPOSITORY_RELATIVE_PATHS}"
    for ((repNum = 0; repNum < ${#REPOSITORY_NAMES[@]}; repNum++)); do

        # concatenate paths to give the correct install path
        # paths are defined in ./projectSettings.sh - insert path-separator as required
        thisRep=${REPOSITORY_NAMES[${repNum}]}
        relPath=${REPOSITORY_RELATIVE_PATHS[${repNum}]}
        thisPath=${PROJECT_DEFAULT}${relPath:+/}${relPath}
        thisSource=${REPOSITORY_SOURCE[${repNum}]}

        if [[ -d "${thisPath}" ]]; then
            # cloning into an already existing path will cause fatal git error
            # if the path already exists, it will be moved to path with date/time extension
            # for nested repositories, the top-level may move everything
            if [[ ${existing} == "yes" ]]; then
                dateTime=$(date '+%d-%m-%Y_%H:%M:%S')
                oldPath=${thisPath}_${dateTime}

                echo "Repository already exists, it will be moved to ${oldPath}"
                ${DRYRUN} mv "${thisPath}" "${oldPath}" || exit

                # clone the repository
                echo "Cloning repository into ${thisPath}"
                ${DRYRUN} git clone "${thisSource}/${thisRep}".git "${thisPath}"
            else
                echo "Skipping repository ${thisPath}"
            fi
        else
            # clone the repository
            echo "Cloning repository into ${thisPath}"
            ${DRYRUN} git clone "${thisSource}/${thisRep}".git "${thisPath}"
        fi
    done
fi

# checkout the required release
if [[ ${gitBranch} ]]; then
    echo "Switching repositories to branch ${gitBranch}"

    for ((repNum = 0; repNum < ${#REPOSITORY_NAMES[@]}; repNum++)); do
        # concatenate paths to give the correct install path
        thisRep=${REPOSITORY_NAMES[${repNum}]}
        thisPath=${PROJECT_DEFAULT}/${REPOSITORY_RELATIVE_PATHS[${repNum}]}
        thisSource=${REPOSITORY_SOURCE[${repNum}]}

        if [[ -d "${thisPath}" ]]; then
            cd "${thisPath}" || exit
            echo "Fetching branch ${thisPath}"
            ${DRYRUN} git fetch --all

            # only checkout if the branch exists
            if [[ $(${DRYRUN} git ls-remote --heads "${thisSource}/${thisRep}".git "${gitBranch}" | wc -l) -eq "1" ]]; then
                echo "Checkout path ${thisPath} to branch ${gitBranch} - using ${remoteSource} as tracked remote"
                ${DRYRUN} git checkout -B "${gitBranch}" --track "${remoteSource}/${gitBranch}"
                echo "Resetting branch ${thisPath} to ${remoteSource}/${gitBranch}"
                ${DRYRUN} git reset --hard "${remoteSource}/${gitBranch}"
            else
                echo "Branch doesn't exists: ${thisSource}/${thisRep}.git ${gitBranch}"
                echo "trying ${GIT_DEFAULT}"

                if [[ $(${DRYRUN} git ls-remote --heads "${thisSource}/${thisRep}".git "${GIT_DEFAULT}" | wc -l) -eq "1" ]]; then
                    echo "Checkout path ${thisPath} to branch ${GIT_DEFAULT} - using ${remoteSource} as tracked remote"
                    ${DRYRUN} git checkout -B "${GIT_DEFAULT}" --track "${remoteSource}/${GIT_DEFAULT}"
                    echo "Resetting branch ${thisPath} to ${remoteSource}/${GIT_DEFAULT}"
                    ${DRYRUN} git reset --hard "${remoteSource}/${GIT_DEFAULT}"
                else
                    echo "Branch doesn't exists: ${thisSource}/${thisRep}.git ${GIT_DEFAULT}"
                fi
            fi
        fi
    done
fi

# create git-hook links as required
if [[ ${githooks} == "yes" ]]; then
    cd "${script_path}" || exit
    ./makeGitHooks.sh ${githookSwitches} || exit
else
    echo "please run ./makeGitHooks.sh to finish"
fi

echo "done"
